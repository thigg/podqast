# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-podqast

CONFIG += sailfishapp_qml

DISTFILES += qml/podqast.qml \
    qml/components/EpisodeContextMenu.qml \
    qml/components/FyydDe.py \
    qml/components/FyydDePython.qml \
    qml/components/LogHandler.qml \
    qml/components/MigrationHandler.qml \
    qml/components/PodqastAudioPlayer.qml \
    lib/hints/SlideShowHint.qml \
    qml/components/hints/HintDockIcons.qml \
    qml/components/hints/HintEpisodeImage.qml \
    qml/components/timeutil.js \
    qml/cover/CoverPage.qml \
    qml/pages/DataMigration.qml \
    qml/pages/FyydSearchPage.qml \
    qml/pages/GpodderSearchPage.qml \
    qml/pages/Log.qml \
    components/hints/MainPageUsageHint.qml \
    components/hints/PlayerHint.qml \
    qml/pages/PodcastDirectorySearchPage.qml \
    lib/hints/PodqastSingleTimeHint.qml \
    qml/pages/SubscribePodcast.qml \
    rpm/podqast.changes.in \
    rpm/podqast.changes.run.in \
    rpm/podqast.spec \
    rpm/podqast.yaml \
    translations/*.ts \
    podqast.desktop \
    qml/params.yml \
    qml/pages/GpodderNetPython.qml \
    images/freak-show.jpg \
    python/feedparser/ \
    python/peewee.py \
    python/playhouse/ \
    qml/params.yml \
    qml/cover/CoverPage.qml \
    qml/pages/GpodderNetPython.qml \
    qml/podqast.qml \
    qml/pages/Podsearchorg.qml \
    qml/pages/Podsearch.qml \
    qml/pages/FeedParserPython.qml \
    qml/pages/Poddescription.qml \
    qml/pages/PodcastListItem.qml \
    qml/pages/Podcastsearch.qml \
    qml/pages/TagListItem.qml \
    qml/pages/DiscoverTags.qml \
    qml/pages/Discover.qml \
    qml/pages/DiscoverUrl.qml \
    qml/pages/PodcastEntryItem.qml \
    qml/pages/IconContextMenu.qml \
    qml/pages/IconContextMenu6.qml \
    qml/pages/PodqastIconMenuItem.qml \
    qml/pages/QueuePostListItem.qml \
    qml/pages/QueueHandlerPython.qml \
    qml/components/ArchivePostListItem.qml \
    qml/pages/History.qml \
    qml/pages/Favorites.qml \
    qml/components/PodcastItem.qml \
    qml/pages/PodpostList.qml \
    qml/pages/Settings.qml \
    qml/pages/PodcastSettings.qml \
    qml/components/InboxHandlerPython.qml \
    qml/pages/PostDescription.qml \
    qml/components/InboxPostListItem.qml \
    qml/components/AppMenu.qml \
    qml/components/FavoriteHandlerPython.qml \
    qml/pages/Chapters.qml \
    qml/components/PodpostPostListItem.qml \
    qml/pages/DiscoverImport.qml \
    qml/pages/About.qml \
    qml/components/PrefAboutMenu.qml \
    qml/pages/Wizzard1.qml \
    qml/pages/Wizzard2.qml \
    qml/pages/FilePicker.qml \
    qml/pages/DiscoverExport.qml \
    qml/pages/External.qml \
    qml/components/ExternalHandlerPython.qml \
    qml/components/BackupButtons.qml

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

QML_IMPORT_PATH +=  "qml/componentspo"

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
TRANSLATIONS += translations/harbour-podqast-de.ts translations/harbour-podqast-sv.ts translations/harbour-podqast-es.ts translations/harbour-podqast-fr.ts translations/harbour-podqast-zh_CN.ts

images.path += /usr/share/harbour-podqast/images
images.files = images/*
python.path = /usr/share/harbour-podqast/python
python.files = python/*

INSTALLS += images python
