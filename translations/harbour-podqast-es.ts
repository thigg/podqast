<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/pages/About.qml" line="9"/>
        <source>../data/about.txt</source>
        <translation>../data/about-es.txt</translation>
    </message>
</context>
<context>
    <name>AppMenu</name>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="9"/>
        <source>Discover</source>
        <translation>Encontrar</translation>
    </message>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="17"/>
        <source>Library</source>
        <translation>Biblioteca</translation>
    </message>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="33"/>
        <source>Playlist</source>
        <translation>Lista de reproducción</translation>
    </message>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="25"/>
        <source>Inbox</source>
        <translation>Entradas</translation>
    </message>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="48"/>
        <source>refreshing: </source>
        <translation>actualizando: </translation>
    </message>
</context>
<context>
    <name>Archive</name>
    <message>
        <location filename="../qml/pages/Archive.qml" line="67"/>
        <source>Library</source>
        <translation>Biblioteca</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="112"/>
        <source>History</source>
        <translation>Historial</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="120"/>
        <source>Favorites</source>
        <translation>Favoritos</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="129"/>
        <source>External Audio</source>
        <translation>Audio externo</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="143"/>
        <source>Rendering</source>
        <translation>Generando</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="144"/>
        <source>Collecting Podcasts</source>
        <translation>Obteniendo podcasts</translation>
    </message>
</context>
<context>
    <name>BackupButtons</name>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="17"/>
        <source>Backup done</source>
        <translation type="unfinished">Copia realizada</translation>
    </message>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="18"/>
        <location filename="../qml/components/BackupButtons.qml" line="19"/>
        <source>Backup done to </source>
        <translation type="unfinished">Copia realizada en </translation>
    </message>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="26"/>
        <source>OPML file saved</source>
        <translation type="unfinished">Archivo OPML guardado</translation>
    </message>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="27"/>
        <location filename="../qml/components/BackupButtons.qml" line="28"/>
        <source>OPML file saved to </source>
        <translation type="unfinished">Archivo OPML guardado en </translation>
    </message>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="36"/>
        <source>Backup</source>
        <translation type="unfinished">Copia de seguridad</translation>
    </message>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="52"/>
        <source>Save to OPML</source>
        <translation type="unfinished">Guardar OPML</translation>
    </message>
</context>
<context>
    <name>Chapters</name>
    <message>
        <location filename="../qml/pages/Chapters.qml" line="49"/>
        <source>Chapters</source>
        <translation>Capítulos</translation>
    </message>
    <message>
        <location filename="../qml/pages/Chapters.qml" line="108"/>
        <source>No chapters</source>
        <translation>No hay capítulos</translation>
    </message>
    <message>
        <location filename="../qml/pages/Chapters.qml" line="109"/>
        <source>Rendering chapters</source>
        <translation>Generando capítulos</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="37"/>
        <source> chapters</source>
        <translation> capítulos</translation>
    </message>
</context>
<context>
    <name>DataMigration</name>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="15"/>
        <source>Update done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="16"/>
        <source>Successfully migrated data to the current version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="41"/>
        <source>Data Migration</source>
        <extracomment>pageheader</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="72"/>
        <source>Because the data format changed, podqast will need to migrate your data to the new format. You can use one of the buttons below to create a backup of your data for recovery.</source>
        <extracomment>general text explaining what the migration does</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="79"/>
        <source>&lt;b&gt;Whats New?&lt;/b&gt;&lt;ul&gt;&lt;li&gt;Some of you lost episodes in the last migration. Because we found this pretty sad, we&apos;re trying to recover them now.&lt;/li&gt;&lt;li&gt;If you are missing old episodes of feeds in general and this didn&apos;t help, try the force refresh button in the settings (We added support for Pagination).&lt;/li&gt;&lt;li&gt;Podcasts are now sorted alphabetically, we hope you like that!&lt;/li&gt;&lt;li&gt;Gpodder should work again.&lt;/li&gt;&lt;li&gt;Check the new Log Page next to the settings.&lt;/li&gt;&lt;li&gt;If you&apos;re streaming an episode and the download finishes, we&apos;re now switching to the downloaded file automatically.&lt;/li&gt;&lt;li&gt;Also more speed, bugfixes and code cleanups.&lt;/li&gt;&lt;/ul&gt;&lt;br&gt;If you want to contribute to podQast, you can help translating the app or report issues on GitLab.</source>
        <extracomment>whatsnew section of the migration</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="86"/>
        <source>The migration is running, please wait.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="98"/>
        <source>Start Migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="124"/>
        <source>Try to ignore errors during migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="136"/>
        <source>We couldnt migrate your data completly. You can restart the app and try it in the non-strict mode. Please consider reporting this as a bug in the Issue-Tracker.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="142"/>
        <source>Copy to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="147"/>
        <source>Open issue tracker</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Discover</name>
    <message>
        <location filename="../qml/pages/Discover.qml" line="30"/>
        <source>Discover</source>
        <translation>Encontrar</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="44"/>
        <source>Search on gpodder.net</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="48"/>
        <source>Search on fyyd.de</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="52"/>
        <source>Tags...</source>
        <translation>Etiquetas...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="56"/>
        <source>Url...</source>
        <translation>Enlace...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="60"/>
        <source>Import...</source>
        <translation>Importar...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="64"/>
        <source>Export...</source>
        <translation>Exportar...</translation>
    </message>
</context>
<context>
    <name>DiscoverExport</name>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="25"/>
        <source>Export</source>
        <translation>Exportar</translation>
    </message>
</context>
<context>
    <name>DiscoverImport</name>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="23"/>
        <source> Podcasts imported</source>
        <translation> podcasts importados</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="25"/>
        <location filename="../qml/pages/DiscoverImport.qml" line="27"/>
        <source> Podcasts imported from OPML</source>
        <translation> podcasts importados desde OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="49"/>
        <source>Discover by Importing</source>
        <translation>Encontrar con importación</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="62"/>
        <source>Pick an OPML file</source>
        <translation>Elige un archivo OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="63"/>
        <source>OPML file</source>
        <translation>Archivo OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="83"/>
        <source>Import OPML</source>
        <translation>Importar OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="85"/>
        <source>Import OPML File</source>
        <translation>Importar archivo OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="105"/>
        <source>Import from Gpodder</source>
        <translation>Importar desde Gpodder</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="107"/>
        <source>Import Gpodder Database</source>
        <translation>Importar base de datos Gpodder</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="131"/>
        <source>Please note: Importing does take some time!</source>
        <translation>Ten en cuenta: ¡la importación tarda un rato!</translation>
    </message>
</context>
<context>
    <name>DiscoverTags</name>
    <message>
        <location filename="../qml/pages/DiscoverTags.qml" line="43"/>
        <source>Discover Tags</source>
        <translation>Encontrar con etiquetas</translation>
    </message>
</context>
<context>
    <name>DiscoverUrl</name>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="29"/>
        <source>Discover by URL</source>
        <translation>Encontrar con URL</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="49"/>
        <source>Enter podcasts&apos; feed url</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="67"/>
        <source>Please enter feed urls here - not web page urls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="80"/>
        <source>Discover</source>
        <translation>Encontrar</translation>
    </message>
</context>
<context>
    <name>EpisodeContextMenu</name>
    <message>
        <location filename="../qml/components/EpisodeContextMenu.qml" line="96"/>
        <source>Select chapters</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>External</name>
    <message>
        <location filename="../qml/pages/External.qml" line="52"/>
        <source>External Audio</source>
        <translation>Audio externo</translation>
    </message>
    <message>
        <location filename="../qml/pages/External.qml" line="69"/>
        <source>Rendering</source>
        <translation>Generando</translation>
    </message>
    <message>
        <location filename="../qml/pages/External.qml" line="70"/>
        <source>Collecting Posts</source>
        <translation>Recopilando publicaciones</translation>
    </message>
</context>
<context>
    <name>Favorites</name>
    <message>
        <location filename="../qml/pages/Favorites.qml" line="49"/>
        <source>Favorites</source>
        <translation>Favoritos</translation>
    </message>
</context>
<context>
    <name>FeedParserPython</name>
    <message>
        <location filename="../qml/components/FeedParserPython.qml" line="147"/>
        <source>Auto-Post-Limit reached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FeedParserPython.qml" line="148"/>
        <source>for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FeedParserPython.qml" line="149"/>
        <source>Auto-Post-Limit reached for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FeedParserPython.qml" line="157"/>
        <source>Error</source>
        <translation type="unfinished">Error</translation>
    </message>
</context>
<context>
    <name>History</name>
    <message>
        <location filename="../qml/pages/History.qml" line="46"/>
        <source>History</source>
        <translation>Historial</translation>
    </message>
    <message>
        <location filename="../qml/pages/History.qml" line="63"/>
        <source>Rendering</source>
        <translation>Generando</translation>
    </message>
    <message>
        <location filename="../qml/pages/History.qml" line="64"/>
        <source>Collecting Posts</source>
        <translation>Recopilando publicaciones</translation>
    </message>
</context>
<context>
    <name>Inbox</name>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="57"/>
        <source>Inbox</source>
        <translation>Entradas</translation>
    </message>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="68"/>
        <source>Moving all posts to archive</source>
        <translation>Mover todas las publicaciones al almacén</translation>
    </message>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="91"/>
        <source>No new posts</source>
        <translation>No hay nuevas publicaciones</translation>
    </message>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="92"/>
        <source>Pull down to Discover new podcasts, get posts from Library, or play the Playlist</source>
        <translation>Desliza hacia abajo para encontrar nuevos podcasts, ver publicaciones de la biblioteca, o reproducir la lista de reproducción</translation>
    </message>
</context>
<context>
    <name>Log</name>
    <message>
        <location filename="../qml/pages/Log.qml" line="25"/>
        <source>Logs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Log.qml" line="63"/>
        <source>No logs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Log.qml" line="74"/>
        <source>Skipped refresh on &apos;%1&apos;, because the server responded with &apos;Not Modified&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Log.qml" line="77"/>
        <source>Updated &apos;%1&apos;. %2 new episodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Log.qml" line="80"/>
        <source>&apos;%1&apos; could not be refreshed because: %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPageUsageHint</name>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="63"/>
        <source>You can see whats going on by looking into the log down here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="68"/>
        <source>Podqast supports pagination of feeds. If you are missing old episodes of feeds, you can find a force refresh button in the settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="21"/>
        <source>On an episode image, this icon shows that it is downloaded...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="9"/>
        <source>Hi there, I gonna give you a quick tour to show you basic and new features.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="15"/>
        <source>Quick touch on an episode to see its description, long press it to add it to queue or play it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="28"/>
        <source>...currently playing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="35"/>
        <source>...already listened.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="42"/>
        <source>In the dock below, this icon means the file currently played is streaming from the internet right now...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="49"/>
        <source>...the sleeptimer is active</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="56"/>
        <source>...a playrate different from 1 is set.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Player</name>
    <message>
        <location filename="../qml/pages/Player.qml" line="15"/>
        <source>Auto-play next episode in queue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="15"/>
        <source>Stop after each episode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="52"/>
        <source>Audio playrate</source>
        <translation>Velocidad de reproducción</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="89"/>
        <source>Sleep timer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="89"/>
        <source> running...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="211"/>
        <source> chapters</source>
        <translation> capítulos</translation>
    </message>
</context>
<context>
    <name>PlayerHint</name>
    <message>
        <location filename="../qml/components/hints/PlayerHint.qml" line="8"/>
        <source>Select if the next item of the playlist should be played automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/PlayerHint.qml" line="13"/>
        <source>Click the image to adjust the playrate</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PodcastDirectorySearchPage</name>
    <message>
        <location filename="../qml/pages/PodcastDirectorySearchPage.qml" line="30"/>
        <source>Discover by Search</source>
        <translation type="unfinished">Encontrar con búsqueda</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastDirectorySearchPage.qml" line="35"/>
        <source>Search</source>
        <translation type="unfinished">Buscar</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastDirectorySearchPage.qml" line="59"/>
        <source>Loading results for &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastDirectorySearchPage.qml" line="63"/>
        <source>No results</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PodcastItem</name>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="21"/>
        <source>Refresh</source>
        <translation>Actualizar</translation>
    </message>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="29"/>
        <source>Settings</source>
        <translation>Ajustes</translation>
    </message>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="37"/>
        <source>Delete</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="43"/>
        <source>Deleting Podcast</source>
        <translation>Borrando podcast</translation>
    </message>
</context>
<context>
    <name>PodcastSettings</name>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="11"/>
        <source>Settings</source>
        <translation type="unfinished">Ajustes</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="45"/>
        <source>URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="57"/>
        <source>Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="39"/>
        <source>%1 Posts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="83"/>
        <source>Move new post to</source>
        <translation>Mover nueva publicación a</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="86"/>
        <source>Inbox</source>
        <translation>Entradas</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="89"/>
        <source>Top of Playlist</source>
        <translation>Al principio de la lista</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="92"/>
        <source>Bottom of Playlist</source>
        <translation>Al final de la lista</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="95"/>
        <source>Archive</source>
        <translation>Almacén</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="102"/>
        <source>Automatic post limit</source>
        <translation>Límite de publicaciones automáticas</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="119"/>
        <source>Audio playrate</source>
        <translation>Velocidad de reproducción</translation>
    </message>
</context>
<context>
    <name>Podcastsearch</name>
    <message>
        <location filename="../qml/pages/Podcastsearch.qml" line="31"/>
        <source>Search by Tag...</source>
        <translation>Buscar por etiqueta...</translation>
    </message>
</context>
<context>
    <name>PodpostList</name>
    <message>
        <location filename="../qml/pages/PodpostList.qml" line="110"/>
        <source>Rendering</source>
        <translation>Generando</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodpostList.qml" line="111"/>
        <source>Creating items</source>
        <translation>Creando elementos</translation>
    </message>
</context>
<context>
    <name>PostDescription</name>
    <message>
        <location filename="../qml/pages/PostDescription.qml" line="26"/>
        <source>Open in browser</source>
        <translation>Abrir en el navegador</translation>
    </message>
</context>
<context>
    <name>PostListItem</name>
    <message>
        <location filename="../qml/components/PostListItem.qml" line="95"/>
        <source>playing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PostListItem.qml" line="99"/>
        <source>listened</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PostListItem.qml" line="103"/>
        <source>remaining</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrefAboutMenu</name>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="8"/>
        <source>Settings</source>
        <translation>Ajustes</translation>
    </message>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="12"/>
        <source>About</source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="17"/>
        <source>Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="22"/>
        <source>Help</source>
        <translation>Ayuda</translation>
    </message>
</context>
<context>
    <name>Queue</name>
    <message>
        <location filename="../qml/pages/Queue.qml" line="65"/>
        <source>Playlist</source>
        <translation>Lista de reproducción</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="81"/>
        <source>No posts</source>
        <translation>No hay publicaciones</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="82"/>
        <source>Pull down to Discover new podcasts or get posts from Inbox or Library</source>
        <translation>Desliza hacia abajo para encontrar nuevos podcasts o ver publicaciones de entradas o biblioteca</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/pages/Settings.qml" line="32"/>
        <source>Global Podcast Settings</source>
        <translation>Ajustes globales de podcast</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="45"/>
        <source>Move new post to</source>
        <translation>Mover nueva publicación a</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="48"/>
        <location filename="../qml/pages/Settings.qml" line="203"/>
        <source>Inbox</source>
        <translation>Entradas</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="57"/>
        <location filename="../qml/pages/Settings.qml" line="212"/>
        <source>Archive</source>
        <translation>Almacén</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="69"/>
        <source>Automatic post limit</source>
        <translation>Límite de publicaciones automáticas</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="95"/>
        <source>Refresh time</source>
        <translation>Tiempo de actualización</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="112"/>
        <source>Audio playrate</source>
        <translation>Velocidad de reproducción</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="99"/>
        <source>off</source>
        <translation>apagado</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="128"/>
        <source>Time left to ignore for listened-mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="223"/>
        <source>Download/Streaming</source>
        <translation>Descarga/Transmisión</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="314"/>
        <source>Development</source>
        <translation>Desarrollo</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="325"/>
        <source>Experimental features</source>
        <translation>Funciones experimentales</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="51"/>
        <location filename="../qml/pages/Settings.qml" line="206"/>
        <source>Top of Playlist</source>
        <translation>Al principio de la lista</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="54"/>
        <location filename="../qml/pages/Settings.qml" line="209"/>
        <source>Bottom of Playlist</source>
        <translation>Al final de la lista</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="73"/>
        <source>All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="132"/>
        <source>s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="144"/>
        <source>Sleep timer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="148"/>
        <source>min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="167"/>
        <source>External Audio</source>
        <translation>Audio externo</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="179"/>
        <source>Allow external audio</source>
        <translation>Permitir audio externo</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="187"/>
        <source>Will take data from
</source>
        <translation>Cogerá datos de</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="200"/>
        <source>Move external audio to</source>
        <translation>Mover audio externo a</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="240"/>
        <source>Download Playlist Posts</source>
        <translation>Descargar publicaciones de la lista</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="249"/>
        <source>Download on Mobile</source>
        <translation>Descargar en el móvil</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="258"/>
        <source>Keep Favorites downloaded</source>
        <translation>Conservar favoritos descargados</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="267"/>
        <source>Will save data in
</source>
        <translation>Guardará los datos en</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="295"/>
        <source>System</source>
        <translation>Sistema</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="306"/>
        <source>Audio viewable in system</source>
        <translation>Audio visible en el sistema</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="158"/>
        <source>Auto-play next episode in queue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="334"/>
        <source>Force refresh of old episodes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SubscribePodcast</name>
    <message>
        <location filename="../qml/pages/SubscribePodcast.qml" line="81"/>
        <source>Could not load feed %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SubscribePodcast.qml" line="170"/>
        <source>Configure</source>
        <translation type="unfinished">Configurar</translation>
    </message>
    <message>
        <location filename="../qml/pages/SubscribePodcast.qml" line="170"/>
        <source>Subscribe</source>
        <translation type="unfinished">Suscribir</translation>
    </message>
    <message>
        <location filename="../qml/pages/SubscribePodcast.qml" line="211"/>
        <source>Feed alternatives</source>
        <translation type="unfinished">Alternativas de canal</translation>
    </message>
    <message>
        <location filename="../qml/pages/SubscribePodcast.qml" line="234"/>
        <source>Latest Post</source>
        <translation type="unfinished">Última publicación</translation>
    </message>
</context>
<context>
    <name>Wizzard1</name>
    <message>
        <location filename="../qml/pages/Wizzard1.qml" line="30"/>
        <source>../data/about.txt</source>
        <translation>../data/about-es.txt</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard1.qml" line="48"/>
        <source>Next</source>
        <translation>Siguiente</translation>
    </message>
</context>
<context>
    <name>Wizzard2</name>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="36"/>
        <source>Let&apos;s start...</source>
        <translation>Empezamos...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="37"/>
        <source>Back to info</source>
        <translation>Volver a info</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="54"/>
        <source> Podcasts imported</source>
        <translation> podcasts importados</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="55"/>
        <location filename="../qml/pages/Wizzard2.qml" line="56"/>
        <source> Podcasts imported from OPML</source>
        <translation> podcasts importados desde OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="73"/>
        <source>You are now able to import old stuff. You can import from Discover lateron. Please note: Importing does take some time!</source>
        <translation>Ahora puedes importar cosas antiguas. Puedes importar más tarde desde &apos;Encontrar&apos;. Ten en cuenta: ¡la importación tarda un rato!</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="102"/>
        <source>Pick an OPML file</source>
        <translation>Elige un archivo OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="103"/>
        <source>OPML file</source>
        <translation>Archivo OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="125"/>
        <source>Import OPML</source>
        <translation>Importar OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="127"/>
        <source>Import OPML File</source>
        <translation>Importar archivo OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="146"/>
        <source>Import from Gpodder</source>
        <translation>Importar desde Gpodder</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="149"/>
        <source>Import Gpodder Database</source>
        <translation>Importar base de datos Gpodder</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="175"/>
        <source>When you now start you can discover new podcasts. After that you can for example select new posts to play from the Library.</source>
        <translation>Cuando comiences ahora puedes encontrar nuevos podcasts. Después puedes seleccionar nuevas publicaciones para reproducir desde la biblioteca.</translation>
    </message>
</context>
<context>
    <name>harbour-podqast</name>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="340"/>
        <source>New posts available</source>
        <translation>Nuevas publicaciones disponibles</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="341"/>
        <source>Click to view updates</source>
        <translation>Haz clic para ver actualizaciones</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="342"/>
        <source>New Posts are available. Click to view.</source>
        <translation>Hay nuevas publicaciones. Haz clic para verlas.</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="359"/>
        <location filename="../qml/harbour-podqast.qml" line="360"/>
        <source>PodQast message</source>
        <translation>Mensaje de PodQast</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="361"/>
        <source>New PodQast message</source>
        <translation>Nuevo mensaje de PodQast</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="381"/>
        <source> Podcasts imported</source>
        <translation> podcasts importados</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="383"/>
        <location filename="../qml/harbour-podqast.qml" line="385"/>
        <source> Podcasts imported from OPML</source>
        <translation> podcasts importados desde OPML</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="451"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="452"/>
        <location filename="../qml/harbour-podqast.qml" line="453"/>
        <source>Audio File not existing</source>
        <translation>El archivo de audio no existe</translation>
    </message>
</context>
</TS>
