<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/pages/About.qml" line="9"/>
        <source>../data/about.txt</source>
        <translation>../data/about-de.txt</translation>
    </message>
</context>
<context>
    <name>AppMenu</name>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="9"/>
        <source>Discover</source>
        <translation>Entdecken</translation>
    </message>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="17"/>
        <source>Library</source>
        <translation>Bibliothek</translation>
    </message>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="33"/>
        <source>Playlist</source>
        <translation>Playlist</translation>
    </message>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="25"/>
        <source>Inbox</source>
        <translation>Eingang</translation>
    </message>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="48"/>
        <source>refreshing: </source>
        <translation type="unfinished">aktualisiere:</translation>
    </message>
</context>
<context>
    <name>Archive</name>
    <message>
        <location filename="../qml/pages/Archive.qml" line="67"/>
        <source>Library</source>
        <translation>Bibliothek</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="112"/>
        <source>History</source>
        <translation>Verlauf</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="120"/>
        <source>Favorites</source>
        <translation>Favoriten</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="129"/>
        <source>External Audio</source>
        <translation>Fremde Audiodateien</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="143"/>
        <source>Rendering</source>
        <translation>Zeichne</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="144"/>
        <source>Collecting Podcasts</source>
        <translation>Sammle Podcasts</translation>
    </message>
</context>
<context>
    <name>BackupButtons</name>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="17"/>
        <source>Backup done</source>
        <translation>Backup durchgeführt</translation>
    </message>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="18"/>
        <location filename="../qml/components/BackupButtons.qml" line="19"/>
        <source>Backup done to </source>
        <translation>Backup durchgeführt nach </translation>
    </message>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="26"/>
        <source>OPML file saved</source>
        <translation>OPML-File gespeichert</translation>
    </message>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="27"/>
        <location filename="../qml/components/BackupButtons.qml" line="28"/>
        <source>OPML file saved to </source>
        <translation>OPML-File gesichert nach </translation>
    </message>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="36"/>
        <source>Backup</source>
        <translation>Backup</translation>
    </message>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="52"/>
        <source>Save to OPML</source>
        <translation>Speichere als OPML</translation>
    </message>
</context>
<context>
    <name>Chapters</name>
    <message>
        <location filename="../qml/pages/Chapters.qml" line="49"/>
        <source>Chapters</source>
        <translation>Kapitel</translation>
    </message>
    <message>
        <location filename="../qml/pages/Chapters.qml" line="108"/>
        <source>No chapters</source>
        <translation>Keine Kapitel</translation>
    </message>
    <message>
        <location filename="../qml/pages/Chapters.qml" line="109"/>
        <source>Rendering chapters</source>
        <translation>Hole Kapitel</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="37"/>
        <source> chapters</source>
        <translation> Kapitel</translation>
    </message>
</context>
<context>
    <name>DataMigration</name>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="15"/>
        <source>Update done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="16"/>
        <source>Successfully migrated data to the current version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="41"/>
        <source>Data Migration</source>
        <extracomment>pageheader</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="72"/>
        <source>Because the data format changed, podqast will need to migrate your data to the new format. You can use one of the buttons below to create a backup of your data for recovery.</source>
        <extracomment>general text explaining what the migration does</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="79"/>
        <source>&lt;b&gt;Whats New?&lt;/b&gt;&lt;ul&gt;&lt;li&gt;Some of you lost episodes in the last migration. Because we found this pretty sad, we&apos;re trying to recover them now.&lt;/li&gt;&lt;li&gt;If you are missing old episodes of feeds in general and this didn&apos;t help, try the force refresh button in the settings (We added support for Pagination).&lt;/li&gt;&lt;li&gt;Podcasts are now sorted alphabetically, we hope you like that!&lt;/li&gt;&lt;li&gt;Gpodder should work again.&lt;/li&gt;&lt;li&gt;Check the new Log Page next to the settings.&lt;/li&gt;&lt;li&gt;If you&apos;re streaming an episode and the download finishes, we&apos;re now switching to the downloaded file automatically.&lt;/li&gt;&lt;li&gt;Also more speed, bugfixes and code cleanups.&lt;/li&gt;&lt;/ul&gt;&lt;br&gt;If you want to contribute to podQast, you can help translating the app or report issues on GitLab.</source>
        <extracomment>whatsnew section of the migration</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="86"/>
        <source>The migration is running, please wait.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="98"/>
        <source>Start Migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="124"/>
        <source>Try to ignore errors during migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="136"/>
        <source>We couldnt migrate your data completly. You can restart the app and try it in the non-strict mode. Please consider reporting this as a bug in the Issue-Tracker.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="142"/>
        <source>Copy to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="147"/>
        <source>Open issue tracker</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Discover</name>
    <message>
        <location filename="../qml/pages/Discover.qml" line="30"/>
        <source>Discover</source>
        <translation>Entdecken</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="44"/>
        <source>Search on gpodder.net</source>
        <translation>Suchen bei gpodder.net</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="48"/>
        <source>Search on fyyd.de</source>
        <translation>Suchen bei fyyd.de</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="52"/>
        <source>Tags...</source>
        <translation>Kategorien…</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="56"/>
        <source>Url...</source>
        <translation>Url...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="60"/>
        <source>Import...</source>
        <translation>Importieren...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="64"/>
        <source>Export...</source>
        <translation>Exportieren...</translation>
    </message>
</context>
<context>
    <name>DiscoverExport</name>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="25"/>
        <source>Export</source>
        <translation>Exportieren</translation>
    </message>
</context>
<context>
    <name>DiscoverImport</name>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="23"/>
        <source> Podcasts imported</source>
        <translation> Podcasts importiert</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="25"/>
        <location filename="../qml/pages/DiscoverImport.qml" line="27"/>
        <source> Podcasts imported from OPML</source>
        <translation> Podcasts vom OPML importiert</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="49"/>
        <source>Discover by Importing</source>
        <translation>Entdecken durch Importieren</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="62"/>
        <source>Pick an OPML file</source>
        <translation>Wähle eine OPML-Datei</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="63"/>
        <source>OPML file</source>
        <translation>Importiere OPML-Datei</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="83"/>
        <source>Import OPML</source>
        <translation>Importiere OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="85"/>
        <source>Import OPML File</source>
        <translation>Importiere OPML-Datei</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="105"/>
        <source>Import from Gpodder</source>
        <translation>Importiere von Gpodder</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="107"/>
        <source>Import Gpodder Database</source>
        <translation>Importiere von Gpodder-Datenbank</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="131"/>
        <source>Please note: Importing does take some time!</source>
        <translation>Beachte: Importieren benötigt etwas Zeit!</translation>
    </message>
</context>
<context>
    <name>DiscoverTags</name>
    <message>
        <location filename="../qml/pages/DiscoverTags.qml" line="43"/>
        <source>Discover Tags</source>
        <translation>Entdecke Kategorien</translation>
    </message>
</context>
<context>
    <name>DiscoverUrl</name>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="29"/>
        <source>Discover by URL</source>
        <translation>Entdecke nach URL</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="49"/>
        <source>Enter podcasts&apos; feed url</source>
        <translation>Gib den URL des Podcast-Feeds ein</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="67"/>
        <source>Please enter feed urls here - not web page urls</source>
        <translation>Gib hier Feed-URLs ein - keine Webseiten-URLs</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="80"/>
        <source>Discover</source>
        <translation>Entdecken</translation>
    </message>
</context>
<context>
    <name>EpisodeContextMenu</name>
    <message>
        <location filename="../qml/components/EpisodeContextMenu.qml" line="96"/>
        <source>Select chapters</source>
        <translation type="unfinished">Kapitel wählen</translation>
    </message>
</context>
<context>
    <name>External</name>
    <message>
        <location filename="../qml/pages/External.qml" line="52"/>
        <source>External Audio</source>
        <translation>Fremde Audiodateien</translation>
    </message>
    <message>
        <location filename="../qml/pages/External.qml" line="69"/>
        <source>Rendering</source>
        <translation>Zeichne</translation>
    </message>
    <message>
        <location filename="../qml/pages/External.qml" line="70"/>
        <source>Collecting Posts</source>
        <translation>Sammle Beiträge</translation>
    </message>
</context>
<context>
    <name>Favorites</name>
    <message>
        <location filename="../qml/pages/Favorites.qml" line="49"/>
        <source>Favorites</source>
        <translation>Favoriten</translation>
    </message>
</context>
<context>
    <name>FeedParserPython</name>
    <message>
        <location filename="../qml/components/FeedParserPython.qml" line="147"/>
        <source>Auto-Post-Limit reached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FeedParserPython.qml" line="148"/>
        <source>for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FeedParserPython.qml" line="149"/>
        <source>Auto-Post-Limit reached for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FeedParserPython.qml" line="157"/>
        <source>Error</source>
        <translation type="unfinished">Fehler</translation>
    </message>
</context>
<context>
    <name>History</name>
    <message>
        <location filename="../qml/pages/History.qml" line="46"/>
        <source>History</source>
        <translation>Verlauf</translation>
    </message>
    <message>
        <location filename="../qml/pages/History.qml" line="63"/>
        <source>Rendering</source>
        <translation>Zeichne</translation>
    </message>
    <message>
        <location filename="../qml/pages/History.qml" line="64"/>
        <source>Collecting Posts</source>
        <translation>Sammle Beiträge</translation>
    </message>
</context>
<context>
    <name>Inbox</name>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="57"/>
        <source>Inbox</source>
        <translation>Eingang</translation>
    </message>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="68"/>
        <source>Moving all posts to archive</source>
        <translation>Lege alle Beiträge ins Archiv</translation>
    </message>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="91"/>
        <source>No new posts</source>
        <translation>Keine neuen Beiträge</translation>
    </message>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="92"/>
        <source>Pull down to Discover new podcasts, get posts from Library, or play the Playlist</source>
        <translation>Menü um neue Podcasts zu Entdecken, hole Beiträge von der Bibliothek oder spiele die Playlist</translation>
    </message>
</context>
<context>
    <name>Log</name>
    <message>
        <location filename="../qml/pages/Log.qml" line="25"/>
        <source>Logs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Log.qml" line="63"/>
        <source>No logs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Log.qml" line="74"/>
        <source>Skipped refresh on &apos;%1&apos;, because the server responded with &apos;Not Modified&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Log.qml" line="77"/>
        <source>Updated &apos;%1&apos;. %2 new episodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Log.qml" line="80"/>
        <source>&apos;%1&apos; could not be refreshed because: %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPageUsageHint</name>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="63"/>
        <source>You can see whats going on by looking into the log down here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="68"/>
        <source>Podqast supports pagination of feeds. If you are missing old episodes of feeds, you can find a force refresh button in the settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="21"/>
        <source>On an episode image, this icon shows that it is downloaded...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="9"/>
        <source>Hi there, I gonna give you a quick tour to show you basic and new features.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="15"/>
        <source>Quick touch on an episode to see its description, long press it to add it to queue or play it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="28"/>
        <source>...currently playing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="35"/>
        <source>...already listened.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="42"/>
        <source>In the dock below, this icon means the file currently played is streaming from the internet right now...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="49"/>
        <source>...the sleeptimer is active</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="56"/>
        <source>...a playrate different from 1 is set.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Player</name>
    <message>
        <location filename="../qml/pages/Player.qml" line="15"/>
        <source>Auto-play next episode in queue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="15"/>
        <source>Stop after each episode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="52"/>
        <source>Audio playrate</source>
        <translation>Abspielgeschwindigkeit</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="89"/>
        <source>Sleep timer</source>
        <translation>Schlummer-Uhr</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="89"/>
        <source> running...</source>
        <translation> läuft...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="211"/>
        <source> chapters</source>
        <translation> Kapitel</translation>
    </message>
</context>
<context>
    <name>PlayerHint</name>
    <message>
        <location filename="../qml/components/hints/PlayerHint.qml" line="8"/>
        <source>Select if the next item of the playlist should be played automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/PlayerHint.qml" line="13"/>
        <source>Click the image to adjust the playrate</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PodcastDirectorySearchPage</name>
    <message>
        <location filename="../qml/pages/PodcastDirectorySearchPage.qml" line="30"/>
        <source>Discover by Search</source>
        <translation type="unfinished">Entdecken durch Suchen</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastDirectorySearchPage.qml" line="35"/>
        <source>Search</source>
        <translation type="unfinished">Suchen</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastDirectorySearchPage.qml" line="59"/>
        <source>Loading results for &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastDirectorySearchPage.qml" line="63"/>
        <source>No results</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PodcastItem</name>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="21"/>
        <source>Refresh</source>
        <translation>Aktualisieren</translation>
    </message>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="29"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="37"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="43"/>
        <source>Deleting Podcast</source>
        <translation>Lösche Podcast</translation>
    </message>
</context>
<context>
    <name>PodcastSettings</name>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="11"/>
        <source>Settings</source>
        <translation type="unfinished">Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="45"/>
        <source>URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="57"/>
        <source>Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="39"/>
        <source>%1 Posts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="83"/>
        <source>Move new post to</source>
        <translation>Neuen Beitrag</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="86"/>
        <source>Inbox</source>
        <translation>Eingang</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="89"/>
        <source>Top of Playlist</source>
        <translation>an den Anfang der Playlist</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="92"/>
        <source>Bottom of Playlist</source>
        <translation>ans Ende der Playlist</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="95"/>
        <source>Archive</source>
        <translation>in das Archiv</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="102"/>
        <source>Automatic post limit</source>
        <translation>Höchstgrenze automatisierter Beiträge</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="119"/>
        <source>Audio playrate</source>
        <translation>Abspielgeschwindigkeit</translation>
    </message>
</context>
<context>
    <name>Podcastsearch</name>
    <message>
        <location filename="../qml/pages/Podcastsearch.qml" line="31"/>
        <source>Search by Tag...</source>
        <translation>Nach Tag suchen...</translation>
    </message>
</context>
<context>
    <name>PodpostList</name>
    <message>
        <location filename="../qml/pages/PodpostList.qml" line="110"/>
        <source>Rendering</source>
        <translation>Zeichne</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodpostList.qml" line="111"/>
        <source>Creating items</source>
        <translation>Erzeuge Elemente</translation>
    </message>
</context>
<context>
    <name>PostDescription</name>
    <message>
        <location filename="../qml/pages/PostDescription.qml" line="26"/>
        <source>Open in browser</source>
        <translation>Im Browser öffnen</translation>
    </message>
</context>
<context>
    <name>PostListItem</name>
    <message>
        <location filename="../qml/components/PostListItem.qml" line="95"/>
        <source>playing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PostListItem.qml" line="99"/>
        <source>listened</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PostListItem.qml" line="103"/>
        <source>remaining</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrefAboutMenu</name>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="8"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="12"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="17"/>
        <source>Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="22"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
</context>
<context>
    <name>Queue</name>
    <message>
        <location filename="../qml/pages/Queue.qml" line="65"/>
        <source>Playlist</source>
        <translation>Playlist</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="81"/>
        <source>No posts</source>
        <translation>Keine Beiträge</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="82"/>
        <source>Pull down to Discover new podcasts or get posts from Inbox or Library</source>
        <translation>Menü um neue Podcasts zu Entdecken oder hole Posts vom Eingang oder der Bibliothek</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/pages/Settings.qml" line="32"/>
        <source>Global Podcast Settings</source>
        <translation>Globale Podcast-Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="45"/>
        <source>Move new post to</source>
        <translation>Neuen Beitrag</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="48"/>
        <location filename="../qml/pages/Settings.qml" line="203"/>
        <source>Inbox</source>
        <translation>in den Eingang</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="57"/>
        <location filename="../qml/pages/Settings.qml" line="212"/>
        <source>Archive</source>
        <translation>in das Archiv</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="69"/>
        <source>Automatic post limit</source>
        <translation>Höchstgrenze automatisierter Beiträge</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="95"/>
        <source>Refresh time</source>
        <translation>Aktualisierungs-Intervall</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="112"/>
        <source>Audio playrate</source>
        <translation>Abspielgeschwindigkeit</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="99"/>
        <source>off</source>
        <translation>aus</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="128"/>
        <source>Time left to ignore for listened-mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="223"/>
        <source>Download/Streaming</source>
        <translation>Laden/Stream</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="314"/>
        <source>Development</source>
        <translation>Entwicklung</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="325"/>
        <source>Experimental features</source>
        <translation>Experimentelle Funktionen</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="51"/>
        <location filename="../qml/pages/Settings.qml" line="206"/>
        <source>Top of Playlist</source>
        <translation>an den Anfang der Playlist</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="54"/>
        <location filename="../qml/pages/Settings.qml" line="209"/>
        <source>Bottom of Playlist</source>
        <translation>ans Ende der Playlist</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="73"/>
        <source>All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="132"/>
        <source>s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="144"/>
        <source>Sleep timer</source>
        <translation>Schlummer-Uhr</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="148"/>
        <source>min</source>
        <translation>min</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="167"/>
        <source>External Audio</source>
        <translation>Fremde Audiodateien</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="179"/>
        <source>Allow external audio</source>
        <translation>Erlaube fremde Audiodateien</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="187"/>
        <source>Will take data from
</source>
        <translation>Hole Daten von
</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="200"/>
        <source>Move external audio to</source>
        <translation>Externe Audiodateien</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="240"/>
        <source>Download Playlist Posts</source>
        <translation>Herunterladen von Playlist-Beiträgen</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="249"/>
        <source>Download on Mobile</source>
        <translation>Bei Mobilfunk laden</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="258"/>
        <source>Keep Favorites downloaded</source>
        <translation>Favoriten speichern</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="267"/>
        <source>Will save data in
</source>
        <translation>Speicher Daten in
</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="295"/>
        <source>System</source>
        <translation>System</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="306"/>
        <source>Audio viewable in system</source>
        <translation>Audio im System sichbar</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="158"/>
        <source>Auto-play next episode in queue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="334"/>
        <source>Force refresh of old episodes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SubscribePodcast</name>
    <message>
        <location filename="../qml/pages/SubscribePodcast.qml" line="81"/>
        <source>Could not load feed %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SubscribePodcast.qml" line="170"/>
        <source>Configure</source>
        <translation type="unfinished">Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/pages/SubscribePodcast.qml" line="170"/>
        <source>Subscribe</source>
        <translation type="unfinished">Abonnieren</translation>
    </message>
    <message>
        <location filename="../qml/pages/SubscribePodcast.qml" line="211"/>
        <source>Feed alternatives</source>
        <translation type="unfinished">Alternative Feeds</translation>
    </message>
    <message>
        <location filename="../qml/pages/SubscribePodcast.qml" line="234"/>
        <source>Latest Post</source>
        <translation type="unfinished">Letzte Sendung</translation>
    </message>
</context>
<context>
    <name>Wizzard1</name>
    <message>
        <location filename="../qml/pages/Wizzard1.qml" line="30"/>
        <source>../data/about.txt</source>
        <translation>../data/about-de.txt</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard1.qml" line="48"/>
        <source>Next</source>
        <translation>Weiter</translation>
    </message>
</context>
<context>
    <name>Wizzard2</name>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="36"/>
        <source>Let&apos;s start...</source>
        <translation>Loslegen...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="37"/>
        <source>Back to info</source>
        <translation>Zurück zur Information</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="54"/>
        <source> Podcasts imported</source>
        <translation> Podcasts importiert</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="55"/>
        <location filename="../qml/pages/Wizzard2.qml" line="56"/>
        <source> Podcasts imported from OPML</source>
        <translation> Podcasts vom OPML importiert</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="73"/>
        <source>You are now able to import old stuff. You can import from Discover lateron. Please note: Importing does take some time!</source>
        <translation>Du kannst nun alte Sachen importieren. Du kannst das auch später über Entdecken machen. Beachte: Importieren benötigt etwas Zeit!</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="102"/>
        <source>Pick an OPML file</source>
        <translation>Wähle eine OPML-Datei</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="103"/>
        <source>OPML file</source>
        <translation>OPML-Datei</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="125"/>
        <source>Import OPML</source>
        <translation>Importiere OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="127"/>
        <source>Import OPML File</source>
        <translation>Importiere OPML-Datei</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="146"/>
        <source>Import from Gpodder</source>
        <translation>Importiere von Gpodder</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="149"/>
        <source>Import Gpodder Database</source>
        <translation>Importiere von Gpodder-Datenbank</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="175"/>
        <source>When you now start you can discover new podcasts. After that you can for example select new posts to play from the Library.</source>
        <translation>Wenn Du weitermachst, kannst Du neue Podcasts aussuchen. Danach kannst Du z. B. neue Posts von der Bibliothek aus aussuchen.</translation>
    </message>
</context>
<context>
    <name>harbour-podqast</name>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="340"/>
        <source>New posts available</source>
        <translation>Neue Beiträge verfügbar</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="341"/>
        <source>Click to view updates</source>
        <translation>Klicken um Aktualisierungen anzusehen</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="342"/>
        <source>New Posts are available. Click to view.</source>
        <translation>Neue Beiträge verfügbar. Klicke zum Betrachten.</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="359"/>
        <location filename="../qml/harbour-podqast.qml" line="360"/>
        <source>PodQast message</source>
        <translation>PodQast-Nachricht</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="361"/>
        <source>New PodQast message</source>
        <translation>Neue PodQast-Nachricht</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="381"/>
        <source> Podcasts imported</source>
        <translation> Podcasts importiert</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="383"/>
        <location filename="../qml/harbour-podqast.qml" line="385"/>
        <source> Podcasts imported from OPML</source>
        <translation> Podcasts vom OPML importiert</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="451"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="452"/>
        <location filename="../qml/harbour-podqast.qml" line="453"/>
        <source>Audio File not existing</source>
        <translation>Audiodatei existiert nicht</translation>
    </message>
</context>
</TS>
