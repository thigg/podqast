<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/pages/About.qml" line="9"/>
        <source>../data/about.txt</source>
        <translation>../data/about-fr.txt</translation>
    </message>
</context>
<context>
    <name>AppMenu</name>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="9"/>
        <source>Discover</source>
        <translation>Découvrir</translation>
    </message>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="17"/>
        <source>Library</source>
        <translation>Bibliothèque</translation>
    </message>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="33"/>
        <source>Playlist</source>
        <translation>Playlist</translation>
    </message>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="25"/>
        <source>Inbox</source>
        <translation>Boîte aux lettres</translation>
    </message>
    <message>
        <location filename="../qml/components/AppMenu.qml" line="48"/>
        <source>refreshing: </source>
        <translation>Actualisation : </translation>
    </message>
</context>
<context>
    <name>Archive</name>
    <message>
        <location filename="../qml/pages/Archive.qml" line="67"/>
        <source>Library</source>
        <translation>Bibliothèque</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="112"/>
        <source>History</source>
        <translation>Historique</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="120"/>
        <source>Favorites</source>
        <translation>Favoris</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="129"/>
        <source>External Audio</source>
        <translation>Fichiers Audios Externes</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="143"/>
        <source>Rendering</source>
        <translation>Chargement</translation>
    </message>
    <message>
        <location filename="../qml/pages/Archive.qml" line="144"/>
        <source>Collecting Podcasts</source>
        <translation>Récupération des Podcasts</translation>
    </message>
</context>
<context>
    <name>BackupButtons</name>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="17"/>
        <source>Backup done</source>
        <translation type="unfinished">Sauvegarde terminée</translation>
    </message>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="18"/>
        <location filename="../qml/components/BackupButtons.qml" line="19"/>
        <source>Backup done to </source>
        <translation type="unfinished">Sauvegarde réalisée dans </translation>
    </message>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="26"/>
        <source>OPML file saved</source>
        <translation type="unfinished">Fichier OPML sauvegardé</translation>
    </message>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="27"/>
        <location filename="../qml/components/BackupButtons.qml" line="28"/>
        <source>OPML file saved to </source>
        <translation type="unfinished">Fichier OPML sauvegardé dans </translation>
    </message>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="36"/>
        <source>Backup</source>
        <translation type="unfinished">Faire une sauvegarde</translation>
    </message>
    <message>
        <location filename="../qml/components/BackupButtons.qml" line="52"/>
        <source>Save to OPML</source>
        <translation type="unfinished">Sauvegarder au format OPML</translation>
    </message>
</context>
<context>
    <name>Chapters</name>
    <message>
        <location filename="../qml/pages/Chapters.qml" line="49"/>
        <source>Chapters</source>
        <translation>Chapitres</translation>
    </message>
    <message>
        <location filename="../qml/pages/Chapters.qml" line="108"/>
        <source>No chapters</source>
        <translation>Aucun chapitre</translation>
    </message>
    <message>
        <location filename="../qml/pages/Chapters.qml" line="109"/>
        <source>Rendering chapters</source>
        <translation>Génération des chapitres</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="37"/>
        <source> chapters</source>
        <translation> chapitres</translation>
    </message>
</context>
<context>
    <name>DataMigration</name>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="15"/>
        <source>Update done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="16"/>
        <source>Successfully migrated data to the current version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="41"/>
        <source>Data Migration</source>
        <extracomment>pageheader</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="72"/>
        <source>Because the data format changed, podqast will need to migrate your data to the new format. You can use one of the buttons below to create a backup of your data for recovery.</source>
        <extracomment>general text explaining what the migration does</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="79"/>
        <source>&lt;b&gt;Whats New?&lt;/b&gt;&lt;ul&gt;&lt;li&gt;Some of you lost episodes in the last migration. Because we found this pretty sad, we&apos;re trying to recover them now.&lt;/li&gt;&lt;li&gt;If you are missing old episodes of feeds in general and this didn&apos;t help, try the force refresh button in the settings (We added support for Pagination).&lt;/li&gt;&lt;li&gt;Podcasts are now sorted alphabetically, we hope you like that!&lt;/li&gt;&lt;li&gt;Gpodder should work again.&lt;/li&gt;&lt;li&gt;Check the new Log Page next to the settings.&lt;/li&gt;&lt;li&gt;If you&apos;re streaming an episode and the download finishes, we&apos;re now switching to the downloaded file automatically.&lt;/li&gt;&lt;li&gt;Also more speed, bugfixes and code cleanups.&lt;/li&gt;&lt;/ul&gt;&lt;br&gt;If you want to contribute to podQast, you can help translating the app or report issues on GitLab.</source>
        <extracomment>whatsnew section of the migration</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="86"/>
        <source>The migration is running, please wait.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="98"/>
        <source>Start Migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="124"/>
        <source>Try to ignore errors during migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="136"/>
        <source>We couldnt migrate your data completly. You can restart the app and try it in the non-strict mode. Please consider reporting this as a bug in the Issue-Tracker.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="142"/>
        <source>Copy to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/DataMigration.qml" line="147"/>
        <source>Open issue tracker</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Discover</name>
    <message>
        <location filename="../qml/pages/Discover.qml" line="30"/>
        <source>Discover</source>
        <translation>Découvrir</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="44"/>
        <source>Search on gpodder.net</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="48"/>
        <source>Search on fyyd.de</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="52"/>
        <source>Tags...</source>
        <translation>Tags...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="56"/>
        <source>Url...</source>
        <translation>Url...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="60"/>
        <source>Import...</source>
        <translation>Importer...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Discover.qml" line="64"/>
        <source>Export...</source>
        <translation>Exporter...</translation>
    </message>
</context>
<context>
    <name>DiscoverExport</name>
    <message>
        <location filename="../qml/pages/DiscoverExport.qml" line="25"/>
        <source>Export</source>
        <translation>Exporter</translation>
    </message>
</context>
<context>
    <name>DiscoverImport</name>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="23"/>
        <source> Podcasts imported</source>
        <translation>Podcasts importés</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="25"/>
        <location filename="../qml/pages/DiscoverImport.qml" line="27"/>
        <source> Podcasts imported from OPML</source>
        <translation>Podcasts importés au format OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="49"/>
        <source>Discover by Importing</source>
        <translation>Découvrir en Important</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="62"/>
        <source>Pick an OPML file</source>
        <translation>Choisissez un fichier OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="63"/>
        <source>OPML file</source>
        <translation>Fichier OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="83"/>
        <source>Import OPML</source>
        <translation>Importer de l&apos;OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="85"/>
        <source>Import OPML File</source>
        <translation>Importer un fichier OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="105"/>
        <source>Import from Gpodder</source>
        <translation>Importer depuis Gpodder</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="107"/>
        <source>Import Gpodder Database</source>
        <translation>Importer une BDD Gpodder</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverImport.qml" line="131"/>
        <source>Please note: Importing does take some time!</source>
        <translation>Attention : notez qu&apos;un import peut prendre
        du temps !</translation>
    </message>
</context>
<context>
    <name>DiscoverTags</name>
    <message>
        <location filename="../qml/pages/DiscoverTags.qml" line="43"/>
        <source>Discover Tags</source>
        <translation>Découvrir par Tags</translation>
    </message>
</context>
<context>
    <name>DiscoverUrl</name>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="29"/>
        <source>Discover by URL</source>
        <translation>Découvrir par URL</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="49"/>
        <source>Enter podcasts&apos; feed url</source>
        <translation>Insérer le flux URL contenant les podcasts</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="67"/>
        <source>Please enter feed urls here - not web page urls</source>
        <translation>N&apos;insérer un lien que vers le flux, non vers le site web</translation>
    </message>
    <message>
        <location filename="../qml/pages/DiscoverUrl.qml" line="80"/>
        <source>Discover</source>
        <translation>Découvrir</translation>
    </message>
</context>
<context>
    <name>EpisodeContextMenu</name>
    <message>
        <location filename="../qml/components/EpisodeContextMenu.qml" line="96"/>
        <source>Select chapters</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>External</name>
    <message>
        <location filename="../qml/pages/External.qml" line="52"/>
        <source>External Audio</source>
        <translation>Fichiers Audios Externes</translation>
    </message>
    <message>
        <location filename="../qml/pages/External.qml" line="69"/>
        <source>Rendering</source>
        <translation>Chargement</translation>
    </message>
    <message>
        <location filename="../qml/pages/External.qml" line="70"/>
        <source>Collecting Posts</source>
        <translation>Récupération des Articles</translation>
    </message>
</context>
<context>
    <name>Favorites</name>
    <message>
        <location filename="../qml/pages/Favorites.qml" line="49"/>
        <source>Favorites</source>
        <translation>Favoris</translation>
    </message>
</context>
<context>
    <name>FeedParserPython</name>
    <message>
        <location filename="../qml/components/FeedParserPython.qml" line="147"/>
        <source>Auto-Post-Limit reached</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FeedParserPython.qml" line="148"/>
        <source>for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FeedParserPython.qml" line="149"/>
        <source>Auto-Post-Limit reached for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/FeedParserPython.qml" line="157"/>
        <source>Error</source>
        <translation type="unfinished">Erreur</translation>
    </message>
</context>
<context>
    <name>History</name>
    <message>
        <location filename="../qml/pages/History.qml" line="46"/>
        <source>History</source>
        <translation>Historique</translation>
    </message>
    <message>
        <location filename="../qml/pages/History.qml" line="63"/>
        <source>Rendering</source>
        <translation>Chargement</translation>
    </message>
    <message>
        <location filename="../qml/pages/History.qml" line="64"/>
        <source>Collecting Posts</source>
        <translation>Récupération des Articles</translation>
    </message>
</context>
<context>
    <name>Inbox</name>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="57"/>
        <source>Inbox</source>
        <translation>Boîte aux lettres</translation>
    </message>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="68"/>
        <source>Moving all posts to archive</source>
        <translation>Transfert des courriers aux archives</translation>
    </message>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="91"/>
        <source>No new posts</source>
        <translation>Aucun nouveau courrier</translation>
    </message>
    <message>
        <location filename="../qml/pages/Inbox.qml" line="92"/>
        <source>Pull down to Discover new podcasts, get posts from Library, or play the Playlist</source>
        <translation>Glisser vers le bas pour Découvrir de nouveaux podcasts, obtenir de nouveaux courriers de la Bibliothèque, ou écouter la Playlist</translation>
    </message>
</context>
<context>
    <name>Log</name>
    <message>
        <location filename="../qml/pages/Log.qml" line="25"/>
        <source>Logs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Log.qml" line="63"/>
        <source>No logs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Log.qml" line="74"/>
        <source>Skipped refresh on &apos;%1&apos;, because the server responded with &apos;Not Modified&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Log.qml" line="77"/>
        <source>Updated &apos;%1&apos;. %2 new episodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Log.qml" line="80"/>
        <source>&apos;%1&apos; could not be refreshed because: %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPageUsageHint</name>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="63"/>
        <source>You can see whats going on by looking into the log down here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="68"/>
        <source>Podqast supports pagination of feeds. If you are missing old episodes of feeds, you can find a force refresh button in the settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="21"/>
        <source>On an episode image, this icon shows that it is downloaded...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="9"/>
        <source>Hi there, I gonna give you a quick tour to show you basic and new features.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="15"/>
        <source>Quick touch on an episode to see its description, long press it to add it to queue or play it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="28"/>
        <source>...currently playing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="35"/>
        <source>...already listened.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="42"/>
        <source>In the dock below, this icon means the file currently played is streaming from the internet right now...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="49"/>
        <source>...the sleeptimer is active</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/MainPageUsageHint.qml" line="56"/>
        <source>...a playrate different from 1 is set.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Player</name>
    <message>
        <location filename="../qml/pages/Player.qml" line="15"/>
        <source>Auto-play next episode in queue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="15"/>
        <source>Stop after each episode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="52"/>
        <source>Audio playrate</source>
        <translation>Vitesse de lecture</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="89"/>
        <source>Sleep timer</source>
        <translation>Minuterie avant coupure</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="89"/>
        <source> running...</source>
        <translation> activée...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Player.qml" line="211"/>
        <source> chapters</source>
        <translation> chapitres</translation>
    </message>
</context>
<context>
    <name>PlayerHint</name>
    <message>
        <location filename="../qml/components/hints/PlayerHint.qml" line="8"/>
        <source>Select if the next item of the playlist should be played automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/hints/PlayerHint.qml" line="13"/>
        <source>Click the image to adjust the playrate</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PodcastDirectorySearchPage</name>
    <message>
        <location filename="../qml/pages/PodcastDirectorySearchPage.qml" line="30"/>
        <source>Discover by Search</source>
        <translation type="unfinished">Découvrir en Recherchant</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastDirectorySearchPage.qml" line="35"/>
        <source>Search</source>
        <translation type="unfinished">Rechercher</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastDirectorySearchPage.qml" line="59"/>
        <source>Loading results for &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastDirectorySearchPage.qml" line="63"/>
        <source>No results</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PodcastItem</name>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="21"/>
        <source>Refresh</source>
        <translation>Rafraichir</translation>
    </message>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="29"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="37"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../qml/components/PodcastItem.qml" line="43"/>
        <source>Deleting Podcast</source>
        <translation>Suppression du Podcast</translation>
    </message>
</context>
<context>
    <name>PodcastSettings</name>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="11"/>
        <source>Settings</source>
        <translation type="unfinished">Paramètres</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="45"/>
        <source>URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="57"/>
        <source>Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="39"/>
        <source>%1 Posts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="83"/>
        <source>Move new post to</source>
        <translation>Transférer les nouveaux articles</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="86"/>
        <source>Inbox</source>
        <translation>Dans la Boîte aux lettres</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="89"/>
        <source>Top of Playlist</source>
        <translation>En début de Playlist</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="92"/>
        <source>Bottom of Playlist</source>
        <translation>En fin de playlist</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="95"/>
        <source>Archive</source>
        <translation>Aux Archives</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="102"/>
        <source>Automatic post limit</source>
        <translation>Limite automatique du nombre d&apos;articles</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodcastSettings.qml" line="119"/>
        <source>Audio playrate</source>
        <translation>Vitesse de lecture</translation>
    </message>
</context>
<context>
    <name>Podcastsearch</name>
    <message>
        <location filename="../qml/pages/Podcastsearch.qml" line="31"/>
        <source>Search by Tag...</source>
        <translation>Rechercher par Tag...</translation>
    </message>
</context>
<context>
    <name>PodpostList</name>
    <message>
        <location filename="../qml/pages/PodpostList.qml" line="110"/>
        <source>Rendering</source>
        <translation>Chargement</translation>
    </message>
    <message>
        <location filename="../qml/pages/PodpostList.qml" line="111"/>
        <source>Creating items</source>
        <translation>Génération des éléments</translation>
    </message>
</context>
<context>
    <name>PostDescription</name>
    <message>
        <location filename="../qml/pages/PostDescription.qml" line="26"/>
        <source>Open in browser</source>
        <translation>Ouvrir dans le navigateur</translation>
    </message>
</context>
<context>
    <name>PostListItem</name>
    <message>
        <location filename="../qml/components/PostListItem.qml" line="95"/>
        <source>playing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PostListItem.qml" line="99"/>
        <source>listened</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PostListItem.qml" line="103"/>
        <source>remaining</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrefAboutMenu</name>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="8"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="12"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="17"/>
        <source>Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PrefAboutMenu.qml" line="22"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
</context>
<context>
    <name>Queue</name>
    <message>
        <location filename="../qml/pages/Queue.qml" line="65"/>
        <source>Playlist</source>
        <translation>File d&apos;attente</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="81"/>
        <source>No posts</source>
        <translation>Aucun article</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="82"/>
        <source>Pull down to Discover new podcasts or get posts from Inbox or Library</source>
        <translation>Glisser vers le bas pour Découvrir de nouveaux podcasts ou obtenir de nouveaux articles de la Boîte aux lettres ou de la Bibliothèque</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/pages/Settings.qml" line="32"/>
        <source>Global Podcast Settings</source>
        <translation>Paramètres Globaux des Podcasts</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="45"/>
        <source>Move new post to</source>
        <translation>Transférer les nouveaux articles</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="48"/>
        <location filename="../qml/pages/Settings.qml" line="203"/>
        <source>Inbox</source>
        <translation>Boîte aux lettres</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="57"/>
        <location filename="../qml/pages/Settings.qml" line="212"/>
        <source>Archive</source>
        <translation>Archives</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="69"/>
        <source>Automatic post limit</source>
        <translation>Limite automatique du nombre d&apos;articles</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="95"/>
        <source>Refresh time</source>
        <translation>Temps de rafraîchissement</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="112"/>
        <source>Audio playrate</source>
        <translation>Vitesse de lecture</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="99"/>
        <source>off</source>
        <translation>Désactivé</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="128"/>
        <source>Time left to ignore for listened-mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="223"/>
        <source>Download/Streaming</source>
        <translation>Téléchargement/Streaming</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="314"/>
        <source>Development</source>
        <translation>Développement</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="325"/>
        <source>Experimental features</source>
        <translation>Fonctionnalités expérimentales</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="51"/>
        <location filename="../qml/pages/Settings.qml" line="206"/>
        <source>Top of Playlist</source>
        <translation>En début de Playlist</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="54"/>
        <location filename="../qml/pages/Settings.qml" line="209"/>
        <source>Bottom of Playlist</source>
        <translation>En fin de la Playlist</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="73"/>
        <source>All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="132"/>
        <source>s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="144"/>
        <source>Sleep timer</source>
        <translation>Minuterie avant coupure</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="148"/>
        <source>min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="167"/>
        <source>External Audio</source>
        <translation>Fichiers Audios Externes</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="179"/>
        <source>Allow external audio</source>
        <translation>Autoriser les fichiers externes</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="187"/>
        <source>Will take data from
</source>
        <translation>Prendra les données depuis
</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="200"/>
        <source>Move external audio to</source>
        <translation>Transférer les fichiers externes</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="240"/>
        <source>Download Playlist Posts</source>
        <translation>Télécharger les Playlists d&apos;Articles</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="249"/>
        <source>Download on Mobile</source>
        <translation>Télécharger hors Wifi</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="258"/>
        <source>Keep Favorites downloaded</source>
        <translation>Conserver les favoris déjà téléchargés</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="267"/>
        <source>Will save data in
</source>
        <translation>Sauvegardera les données dans
</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="295"/>
        <source>System</source>
        <translation>Système</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="306"/>
        <source>Audio viewable in system</source>
        <translation>Audio visible dans le système</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="158"/>
        <source>Auto-play next episode in queue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="334"/>
        <source>Force refresh of old episodes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SubscribePodcast</name>
    <message>
        <location filename="../qml/pages/SubscribePodcast.qml" line="81"/>
        <source>Could not load feed %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SubscribePodcast.qml" line="170"/>
        <source>Configure</source>
        <translation type="unfinished">Configurer</translation>
    </message>
    <message>
        <location filename="../qml/pages/SubscribePodcast.qml" line="170"/>
        <source>Subscribe</source>
        <translation type="unfinished">S&apos;abonner</translation>
    </message>
    <message>
        <location filename="../qml/pages/SubscribePodcast.qml" line="211"/>
        <source>Feed alternatives</source>
        <translation type="unfinished">Flux Alternatifs</translation>
    </message>
    <message>
        <location filename="../qml/pages/SubscribePodcast.qml" line="234"/>
        <source>Latest Post</source>
        <translation type="unfinished">Derniers Articles</translation>
    </message>
</context>
<context>
    <name>Wizzard1</name>
    <message>
        <location filename="../qml/pages/Wizzard1.qml" line="30"/>
        <source>../data/about.txt</source>
        <translation>../data/about.txt</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard1.qml" line="48"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
</context>
<context>
    <name>Wizzard2</name>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="36"/>
        <source>Let&apos;s start...</source>
        <translation>Commençons...</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="37"/>
        <source>Back to info</source>
        <translation>Retourner aux informations</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="54"/>
        <source> Podcasts imported</source>
        <translation> Podcasts importés</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="55"/>
        <location filename="../qml/pages/Wizzard2.qml" line="56"/>
        <source> Podcasts imported from OPML</source>
        <translation> Podcasts importés de l&apos;OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="73"/>
        <source>You are now able to import old stuff. You can import from Discover lateron. Please note: Importing does take some time!</source>
        <translation>Vous ne pouvez plus importer de vieux trucs. Vous pouvez cependant importer depuis Découvrir dorénavant. Notez que l&apos;import peut prendre du temps !</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="102"/>
        <source>Pick an OPML file</source>
        <translation>Choisissez un fichier OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="103"/>
        <source>OPML file</source>
        <translation>Fichier OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="125"/>
        <source>Import OPML</source>
        <translation>Importer de l&apos;OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="127"/>
        <source>Import OPML File</source>
        <translation>Importer un fichier OPML</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="146"/>
        <source>Import from Gpodder</source>
        <translation>Importer depuis Gpodder</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="149"/>
        <source>Import Gpodder Database</source>
        <translation>Importer une base de données Gpodder</translation>
    </message>
    <message>
        <location filename="../qml/pages/Wizzard2.qml" line="175"/>
        <source>When you now start you can discover new podcasts. After that you can for example select new posts to play from the Library.</source>
        <translation>Vous pouvez maintenant découvrir de nouveaux podcasts. Vous pourrez ensuite les écouter depuis la Bibliothèque.</translation>
    </message>
</context>
<context>
    <name>harbour-podqast</name>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="340"/>
        <source>New posts available</source>
        <translation>Nouveaux articles disponibles</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="341"/>
        <source>Click to view updates</source>
        <translation>Cliquez pour voir les mises à jour</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="342"/>
        <source>New Posts are available. Click to view.</source>
        <translation>De nouveaux articles sont diponibles. Cliquez pour voir.</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="359"/>
        <location filename="../qml/harbour-podqast.qml" line="360"/>
        <source>PodQast message</source>
        <translation>Message PodQast</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="361"/>
        <source>New PodQast message</source>
        <translation>Nouveau message PodQast</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="381"/>
        <source> Podcasts imported</source>
        <translation> Podcasts importés</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="383"/>
        <location filename="../qml/harbour-podqast.qml" line="385"/>
        <source> Podcasts imported from OPML</source>
        <translation> Podcasts importés depuis OPML</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="451"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../qml/harbour-podqast.qml" line="452"/>
        <location filename="../qml/harbour-podqast.qml" line="453"/>
        <source>Audio File not existing</source>
        <translation>Le fichier audio n&apos;existe pas</translation>
    </message>
</context>
</TS>
