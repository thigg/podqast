### Preconditions:
 - .local/harbour-podqast/ does not exist
 - dconf is clean (`dconf reset -f /apps/ControlPanel/podqast/`)

### Testrun:
| step | expected |
| ------ | ------ |
| start app | welcome screen shows |
| do not import anything | on discover screen |
| click search gpodder | opening search|
| type freakshow | showing that it is searching for freakshow. Results do appear|
| select freakshow| showing freakshow with a few topmost episodes and a subscribe button|
|click subscribe|a spinner appears that ends eventually|
|go back and to library| 1) a tutorial appears. clicking forwards to the next step. 2) The steps are not to fast to read 3) freakshow is in the library (can reset tutorial via dconf with `dconf reset -f /apps/ControlPanel/podqast/hints/`|
|go to queue|queue is empty|
|go to inbox| inbox is empty|
|go to player| 1) tutorial appears 2) every step is readable 3) player is empty|
|go to library, select freakshow| episodes do appear|
|long press an episode| menu appears with play, queue top, queue bottom...|
|select play|1) episodes starts, plays with normal speed, 2) the playerdock shows a symbol that indicates that the episode is streaming 3) the download progress is shown on the episode image 4) when download finishes, the streaming indicator disappears and playback is done offline now|
|go to player screen| progress is shown in progress bar and moves|
|click image| menu appears to change playback speed|
|change playback speed to 1.5|episode plays faster|
|pause and play|playback speed stays the same|
|restart the app| 1) episode is still selected in the dock|
|go to player page|1)episode progressbar is where it was before 3) when the playrate slider is shown, it is at its previous position (1.5)|
|play episode|episode plays with 1.5 speed from where it stopped|
|go to library, select freakshow||
|long press other episode and click play| new episode starts playing with 1.5 speed|
|go to playlist|contains 2 episodes|
|long press top one, select arrow down|other episode starts playing where it stopped|
|long press bottom one, select arrow up|other episode starts playing where it stopped|


 - chapters
    - have episode with chapters in queue
    - unselect first chapter
    - play
    - first chapter is skipped
    - skip chapter in player
    - unselect 4th chapter
    - 4th chapter is skipped
    - close app 
    - check if chapter settings are still the same
 - download depending on connection
    - mobile download disabled: working on wifi, not working on mobile
    - enabled: working on wifi and mobile
 - mark as played
    - check setting for mark as played threshold is set properly
 - favorite
    -  mark/unmark as favorite working and reflected in favorite list
 - inbox archive
    - archive inbox working
 - queue archive
    - archive from queue working
 - log
    - log shows sensible infos
 - autoplay disabled
    - enabling/disabling autoplay works
- delete podcast
    - deleting a podcast and adding it again works
 - download removed
    - downloaded episodes are removed properly from store after removal from queue
 - backup working
    - backing up and restore working with opml and tar
  - open podcast, search for episode is working properly (click on search icon in dock)
    - type searchterm with 5 letters. Remove searchterm, search and sorting, sorting without search working
  - podcast settings:
    - "move new entries to"  settings are working properly
    - episode url and link are clickable
    - number of posts is shown correctly
  - streaming and faster playback icons in dock are showing correctly
  - external audio is properly shown in the player