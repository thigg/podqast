import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Thumbnailer 1.0

Thumbnail {
    id: listicon
    property int percentage
    anchors.leftMargin: Theme.paddingMedium

    property bool showDownladingState: percentage > 0 && percentage < 100
    property bool showIsExternalAudio
    property bool showIsPlaying
    property bool showIsListened

    MouseArea {
        anchors.fill: parent
        onClicked: {
            pageStack.push(Qt.resolvedUrl("../pages/PodpostList.qml"), {
                               "url": url
                           })
        }
    }
    width: Theme.iconSizeLarge
    height: Theme.iconSizeLarge
    sourceSize.width: Theme.iconSizeLarge
    sourceSize.height: Theme.iconSizeLarge
    source: logo_url == "" ? "../../images/podcast.png" : logo_url
    Rectangle {
        id: dlstatus
        visible: showDownladingState
        anchors.right: parent.right
        height: parent.height
        width: (100 - listicon.percentage) * parent.width / 100
        color: "black"
        opacity: 0.7
        z: 0
    }
    Connections {
        target: queuehandler
        onDownloading: {
            if (dlid == id) {
                listicon.percentage = percent
            }
        }
    }
    Image {
        source: "image://theme/icon-lock-application-update"
        visible: listicon.percentage == 100
        z: 1
        width: parent.width / 4
        height: parent.height / 4
        anchors.bottom: parent.bottom
        anchors.right: parent.right
    }
    Image {
        source: loaded ? "../../images/audio-l.png" : "../../images/audio.png"
        visible: showIsExternalAudio
        z: 1
        width: parent.width / 4
        height: parent.height / 4
        anchors.bottom: parent.bottom
        anchors.right: parent.right
    }
    Image {
        source: "image://theme/icon-lock-voicemail"
        visible: showIsPlaying
        z: 1
        width: parent.width / 4
        height: parent.height / 4
        anchors.top: parent.top
        anchors.left: parent.left
    }
    Image {
        source: "image://theme/icon-lock-installed"
        visible: showIsListened
        z: 1
        width: parent.width / 4
        height: parent.height / 4
        anchors.bottom: parent.bottom
        anchors.left: parent.left
    }
}
