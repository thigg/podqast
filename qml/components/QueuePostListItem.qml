import QtQuick 2.0
import Sailfish.Silica 1.0

PostListItem {

    disableListenedHint: true

    Component.onCompleted: {
        if (moved) {
            openMenu()
        }
    }
    menu: EpisodeContextMenu {
        favoriteEnabled: true
        archiveEnabled: true

        upPressedHandler: function (model) {
            queuehandler.queueMoveUp(model.id)
        }
        downPressedHandler: function (model) {
            queuehandler.queueMoveDown(model.id)
        }
        archivePressedHandler: function (model) {
            queuehandler.queueToArchive(model.id)
        }
    }
}
