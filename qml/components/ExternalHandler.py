# -*- coding: utf-8 -*-
import logging

import pyotherside
import threading
import sys
import os

sys.path.append("/usr/share/harbour-podqast/python")

from podcast.constants import Constants
from podcast.external import ExternalFactory
from podcast.util import movePost

from podcast.podpost import Podpost, PodpostFactory
import inotify.adapters
import mutagen

logger = logging.getLogger(__name__)


def check_new():
    """
    check new files in external_home
    """

    logger.info("Check new/lost audio files")
    external = Constants().external_home
    if not external or not os.path.isdir(external):
        logger.warning("Warning: external directory does not exist!")
        return

    exlib = ExternalFactory().get_external()
    mfiles = []
    for filename in os.listdir(external):
        logging.info("file: %s", filename)
        file_path = os.path.join(external, filename)
        try:
            if not exlib.has_file(filename):
                if os.path.isfile(file_path):
                    wheremove = Constants().get_val("extMoveToVal")
                    if create_external_entry_from_file(file_path, wheremove, filename):
                        mfiles.append(filename)
                    else:
                        logger.warning("Found external file, but could not create podpost %s", file_path)

        except:
            logger.exception("File does not exist")

    for filename, podpost in exlib.get_podposts_objects():
        logger.info("we are on file %s" % filename)
        if not os.path.isfile(os.path.join(external, filename)):
            exlib.remove_podpost(podpost.id)
            pyotherside.send("externalUpdated")


def create_external_entry_from_file(filepath, wheremove, filename):
    try:
        if mutagen.File(filepath):
            logger.info("creating from external file %s", filepath)
            post = Podpost.from_audio_file(filepath)
            PodpostFactory().persist(post)
            exlib = ExternalFactory().get_external()
            exlib.insert(post.id, filename)
            movePost(post.id, wheremove)
            pyotherside.send(
                "updatesNotification", post.title, post.title, "External"
            )
            pyotherside.send("externalUpdated")
            return True
        else:
            logger.warning("Mutagen didnt like file %s", filepath)
    except:
        logger.exception("File %s does not exist", filepath)
        return False


def wait_new():
    """
    check via inotify for new files in ~/podqast/external
    """
    logger.info("starting external watcher")

    external = Constants().external_home
    if not external or not os.path.isdir(external):
        return

    iNotifyer = inotify.adapters.Inotify()
    iNotifyer.add_watch(external)

    fileopened = {}
    exlib = ExternalFactory().get_external()
    while not stop:
        for event in iNotifyer.event_gen(timeout_s=10, yield_nones=False):
            (_, type_names, path, filename) = event
            logger.debug("External inotify got event: %s for '%s' in %s", type_names, filename, path)
            wheremove = Constants().get_val("extMoveToVal")
            filepath = os.path.join(external, filename)
            if "IN_CREATE" in type_names:
                if os.path.islink(filepath):
                    logger.info("Created by ln")
                    create_external_entry_from_file(filepath, wheremove, filename)
                else:
                    logger.debug("Opened file")
                    fileopened[filepath] = True

            if "IN_CLOSE_WRITE" in type_names:
                if filepath in fileopened:
                    logger.info("Created by cp")
                    create_external_entry_from_file(filepath, wheremove, filename)
                    fileopened.pop(filepath)
                else:
                    logger.info("file was not opened, ignoring")

            if "IN_MOVED_TO" in type_names:
                logger.info("Created by mv")
                create_external_entry_from_file(filepath, wheremove, filename)

            if "IN_MOVED_FROM" in type_names:
                logger.info("removed by mv")
                if exlib.has_file(filename):
                    exlib.remove_by_filename(filename)

            if "IN_DELETE" in type_names:
                logger.info("removing file")
                exlib.remove_by_filename(filename)
                pyotherside.send("externalUpdated")
    logger.info("Stopped external watcher. stop: %s", stop)


def get_external_posts():
    """
    Return a list of all external audio posts
    """

    logger.info("collecting external posts")
    external = ExternalFactory().get_external()
    entries = [entry.get_data() for filename, entry in external.get_podposts_objects()]
    pyotherside.send("createExternalList", entries)



def get_audio_data(afile):
    """
    Get info string from mutagen
    """

    try:
        af = mutagen.File(afile)
        pyotherside.send("audioInfo", af.info.pprint())
    except:
        return ""

    return ""


class ExternalHandler:
    def __init__(self):
        self.bgthread = threading.Thread()
        self.bgthread.start()
        self.bgthread2 = threading.Thread()
        self.bgthread2.start()
        self.inothread = threading.Thread()
        self.inothread.start()

    def getexternalposts(self):
        if self.bgthread2.is_alive():
            return
        self.bgthread2 = threading.Thread(target=get_external_posts)
        self.bgthread2.start()

    def waitnew(self):
        if self.inothread.is_alive():
            return
        self.inothread = threading.Thread(target=wait_new)
        self.inothread.start()


stop = False
externalhandler = ExternalHandler()
