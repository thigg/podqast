import QtQuick 2.0
import Sailfish.Silica 1.0

DockedPanel {
    id: playdockedpanel

    width: parent.width
    height: Theme.itemSizeSmall
    open: true

    dock: Dock.Bottom
    default property alias contents: additions.children

    Rectangle {
        anchors.fill: parent
        color: Theme.highlightDimmerColor
        opacity: 0.7
    }

    Row {
        anchors.centerIn: parent
        Row {
            id: row

            //anchors.centerIn: parent
            IconButton {
                icon.source: "image://theme/icon-m-previous"
                onClicked: {
                    playerHandler.fast_backward()
                }
            }
            IconButton {
                icon.source: "image://theme/icon-m-" + (playerHandler.isPlaying ? "pause" : "play")
                onClicked: {
                    playerHandler.playpause()
                }
            }
            IconButton {
                icon.source: "image://theme/icon-m-next"
                onClicked: {
                    playerHandler.fast_forward()
                }
            }
            IconButton {
                id: playpanelicon
                icon.source: playerHandler.playicon
                             === "" ? "../../images/podcast.png" : playerHandler.playicon
                icon.width: Theme.iconSizeMedium
                icon.height: Theme.iconSizeMedium
                icon.color: undefined
                onClicked: {
                    queuehandler.getFirstEntry()
                }
                Connections {
                    target: queuehandler
                    onFirstEntry: {
                        pageStack.push(Qt.resolvedUrl(
                                           "../pages/PostDescription.qml"), {
                                           "title": data.title,
                                           "detail": data.detail,
                                           "length": data.length,
                                           "date": data.date,
                                           "duration": data.duration,
                                           "href": data.link
                                       })
                    }
                }
            }
            IconButton {
                id: podimage
                icon.source: "image://theme/icon-m-right"
                onClicked: pageStack.push(Qt.resolvedUrl("../pages/Player.qml"))
            }
            Column {
                id: statusicons
                Icon {
                    source: "image://theme/icon-s-cloud-download"
                    visible: playerHandler.streaming
                }
                Icon {
                    source: "image://theme/icon-s-timer"
                    visible: podqast.dosleep
                }
                Icon {
                    source: "image://theme/icon-s-duration"
                    visible: playerHandler.playrate !== 1.0
                }
            }
        }

        Item {
            id: placeholder
            height: parent.height
            width: Theme.paddingMedium * 2 + 3
            Rectangle {
                height: parent.height - Theme.paddingMedium * 2
                width: 3
                color: Theme.highlightBackgroundColor
                visible: additions.visibleChildren.length > 0
                anchors.centerIn: parent
            }
        }

        Row {
            id: additions
            LayoutMirroring.enabled: true
        }
    }
}
