function format_duration_ms(duration) {
    return format_duration(duration / 1000)
}

function format_duration(duration) {
    if (!duration) {
        return ''
    }
    var prefix = ""
    if (duration < 0){
        prefix = "-"
        duration = -duration
    }

    var h = parseInt(duration / 3600) % 24
    var m = parseInt(duration / 60) % 60
    var s = parseInt(duration % 60)
    return prefix + ((h > 0) ? lpad(h) + ':' : '') + lpad(m) + ':' + lpad(s)
}

function lpad(s) {
    return (s < 10 ? '0' + s : s)
}
