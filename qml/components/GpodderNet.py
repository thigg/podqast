# -*- coding: utf-8 -*-
import json
import logging

import pyotherside
import threading

import urllib.request
import urllib.parse
from urllib.request import Request

tagslist = []
toplist = []

http = None

def get_toptags():
    """
    Get the Gpodder top tag list
    """

    if tagslist == []:
        toptags = url_get_json("https://www.gpodder.net/api/2/tags/12.json")
        for tag in toptags:
            if tag["usage"] > 70:
                tagslist.append({"tagname": tag["tag"], "position": "TagsList"})
    pyotherside.send("toptags", tagslist)


def url_get_json(url, params=None):
    if params:
        url += "?" + urllib.parse.urlencode(params)
    resource = urllib.request.urlopen(Request(url, method="GET"))
    return json.loads(resource.read().decode("utf-8"))


def get_toplist():
    """
    Get the actual top podcasts (used for Discover grid)
    """

    if toplist == []:
        tops = url_get_json("https://gpodder.net/toplist/20.json")
        for top in tops:
            description = top["description"]
            if len(description) > 160:
                description = description[:160] + "..."

            toplist.append(
                {
                    "title": top["title"],
                    "url": top["url"],
                    "logo_url": top["logo_url"],
                    "description": description,
                }
            )
    pyotherside.send("toplist", toplist)


def get_tags_by_name(tagname):
    """
    Get podcast list by tag name
    """
    pclist = []
    podcasts = url_get_json(f"https://gpodder.net/api/2/tag/{tagname}/20.json")
    for podcast in podcasts:
        title = podcast["title"]
        if len(title) > 50:
            title = title[:50] + "..."
        description = podcast["description"]
        if len(description) > 160:
            description = description[:160] + "..."
        pclist.append(
            {
                "url": podcast["url"],
                "title": title,
                "titlefull": podcast["title"],
                "description": description,
                "website": podcast["website"],
                "logo_url": podcast["logo_url"],
            }
        )
    pyotherside.send("podcastlist", pclist)


def search_pods(query):
    """
    Search for podcast list
    query: the search query
    """
    logging.debug("searching gpodder for '%s'",query)
    pclist = []
    podcasts = url_get_json("https://gpodder.net/search.json", params={"q": query})
    for podcast in podcasts:
        title = podcast["title"]
        if len(title) > 50:
            title = title[:50] + "..."
        description = podcast["description"]
        if len(description) > 160:
            description = description[:160] + "..."
        if not podcast["logo_url"]:
            logo_url = ""
        else:
            logo_url = podcast["logo_url"]

        pclist.append(
            {
                "url": podcast["url"],
                "title": title,
                "titlefull": podcast["title"],
                "description": description,
                "website": podcast["website"],
                "logo_url": logo_url,
            }
        )
    pyotherside.send("podsearch", pclist)


class GpodderNet:
    def __init__(self):
        self.bgthread = threading.Thread()
        self.bgthread.start()

    def gettags(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_toptags)
        self.bgthread.start()

    def getpcbytagname(self, pcname):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(
            target=get_tags_by_name, args=[pcname]
        )
        self.bgthread.start()

    def gettoplist(self):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=get_toplist)
        self.bgthread.start()

    def searchpods(self, query):
        if self.bgthread.is_alive():
            return
        self.bgthread = threading.Thread(target=search_pods, args=[query])
        self.bgthread.start()


gpoddernet = GpodderNet()
