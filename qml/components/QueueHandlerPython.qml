import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4

Python {
    id: queuehandler

    signal createArchiveList(var data)
    signal firstEntry(var data)
    signal setFirst(var data, var chapterlist)
    signal doPlay
    signal posPlay(var position)
    signal setPos(var position)
    signal doStop
    signal createList(var data, string moved)
    signal downloading(string dlid, int percent)
    signal needDownload(string podpost)
    signal firstDownloaded(string filepath)
    signal episodeChapters(var chapters)

    Component.onCompleted: {
        setHandler("createArchiveList", createArchiveList)
        setHandler("firstentry", firstEntry)
        setHandler("setFirst", setFirst)
        setHandler("doplay", doPlay)
        setHandler("posplay", posPlay)
        setHandler("setpos", setPos)
        setHandler("stopped", doStop)
        setHandler("createList", createList)
        setHandler("downloading", downloading)
        setHandler("needDownload", needDownload)
        setHandler("firstDownloaded", firstDownloaded)
        setHandler("episodeChapters", episodeChapters)

        addImportPath(Qt.resolvedUrl("/usr/share/harbour-podqast/python"))
        addImportPath(Qt.resolvedUrl('.'))
        importModule('QueueHandler', function () {
            console.log('QueueHandler is now imported')
        })
    }
    function queueInsertTop(id, callback) {
        call("QueueHandler.instance.queue_insert_top", [id], function () {
            if (callback !== undefined)
                callback()
        })
    }
    function queueInsertNext(id) {
        call("QueueHandler.instance.queue_insert_next", [id], function () {})
    }
    function queueInsertBottom(id) {
        call("QueueHandler.instance.queue_insert_bottom", [id], function () {})
    }
    function queueMoveUp(id) {
        call("QueueHandler.instance.queue_move_up", [id], function () {})
    }

    function queueMoveDown(id) {
        call("QueueHandler.instance.queue_move_down", [id], function () {})
    }

    function queueToArchive(id) {
        call("QueueHandler.instance.queue_to_archive", [id], function () {})
    }
    function queueTopToArchive(playnext) {
        call("QueueHandler.instance.queue_top_to_archive", [playnext],
             function () {})
    }

    function getQueueEntries() {
        call("QueueHandler.instance.get_queue_posts", function () {})
    }
    function getFirstEntry() {
        call("QueueHandler.instance.get_first_entry", function () {})
    }
    function updatePlayingPosition(position) {
        if (position === undefined) {
            position = playerHandler.position
        }
        call("QueueHandler.instance.update_position", [position],
             function () {})
    }
    function downloadAudio(podpost) {
        call("QueueHandler.instance.queue_download", [podpost], function () {})
    }
    function downloadAudioAll() {
        call("QueueHandler.instance.queue_download_all", function () {})
    }

    function getEpisodeChapters(episodeid) {
        call("QueueHandler.instance.get_episode_chapters", [episodeid],
             function () {})
    }

    function sendFirstEpisodeChapters(episodeid) {
        call("QueueHandler.instance.send_first_episode_chapters", [episodeid],
             function () {})
    }

    function toggleChapter(episodeid, chapterid) {
        call("QueueHandler.instance.toggle_chapter", [episodeid, chapterid],
             function () {})
    }

    onError: {
        console.log('python error: ' + traceback)
    }

    onDoStop: {
        console.log("stopped")
        playerHandler.stop()
    }
    onDoPlay: {
        console.log("play")
        playerHandler.play()
    }
    onPosPlay: {
        console.log("posplay")
        playerHandler.seekPos = position
        playerHandler.play()
    }
    onSetPos: {
        console.log("setPos")
        playerHandler.seekPos = position
    }

    onFirstDownloaded: {
        console.log("first Downloaded")
        if (mediaplayer.playbackState == 1) {
            playerHandler.seekPos = -1
            var position = mediaplayer.position
            mediaplayer.stop()
            console.log("we are the first element, switching to file "
                        + filepath + "/" + playerHandler.seekPos)
            //magic needed to replace the source and seek to same position
            mediaplayer.source = "file://" + filepath
            mediaplayer.seek(position)
            mediaplayer.play()
            if (!mediaplayer.seekable) {
                console.log("need to seek indirectly")
                playerHandler.seekPos = position
                playerHandler.doStartAfterSeek = true
            }
        }
    }
}
