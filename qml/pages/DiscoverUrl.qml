import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4
import "../components"

Page {
    id: page
    allowedOrientations: Orientation.All
    property int margin: 16

    SilicaFlickable {
        id: mainflick
        anchors.fill: parent

        AppMenu {
            thispage: "Discover"
        }
        PrefAboutMenu {}

        Row {
            id: discovertitle
            width: page.width
            Column {
                id: column
                width: page.width
                spacing: Theme.paddingLarge

                PageHeader {
                    title: qsTr("Discover by URL")
                }
            }
        }
        Column {
            id: texturlcolumn
            anchors.top: discovertitle.bottom
            width: parent.width
            height: Theme.paddinglarge
            spacing: Theme.paddingMedium
            TextField {
                id: urltext
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }

                width: parent.width
                focus: true
                placeholderText: qsTr("Enter podcasts' feed url")
                inputMethodHints: Qt.ImhUrlCharactersOnly
                EnterKey.enabled: text.trim().length > 3
                EnterKey.iconSource: "image://theme/icon-m-search"
                EnterKey.onClicked: urlEntered()
            }
        }
        Column {
            id: hintcolumn
            height: Theme.itemSizeExtraSmall
            anchors.top: texturlcolumn.bottom
            spacing: Theme.paddingMedium
            Label {
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                text: qsTr("Please enter feed urls here - not web page urls")
                font.pixelSize: Theme.fontSizeExtraSmall
            }
        }
        Column {
            anchors.top: hintcolumn.bottom
            spacing: Theme.paddingMedium
            Button {
                id: discoverButton
                anchors {
                    left: parent.left
                    margins: Theme.paddingMedium
                }
                text: qsTr("Discover")
                onClicked: urlEntered()
            }
        }

        PlayDockedPanel {}
    }

    function urlEntered() {
        var url_string = urltext.text.trim()
        pageStack.push(Qt.resolvedUrl("SubscribePodcast.qml"), {
                           "url": url_string
                       })
    }
}
