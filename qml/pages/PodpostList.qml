import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page
    property var url
    property var link
    property var title
    property bool podpostslist: true

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    Component.onCompleted: {
        get_data()
        feedparserhandler.getPodcast(url)
    }

    property string searchtext: ""
    property string order: "published"
    property string order_dir: "desc"
    property bool fav_filter: false
    // 2 do not filter, 0 not listened, 1 listened
    property int listened_filter: 2

    function get_data() {
        var get_options = {
            "podcast_url": url,
            "searchtext": searchtext,
            "fav_filter": fav_filter,
            "order": order,
            "order_dir": order_dir
        }
        if (listened_filter < 2)
            get_options["listened_filter"] = listened_filter === 1
        feedparserhandler.getEntries(get_options)
    }

    Connections {
        target: feedparserhandler
        ignoreUnknownSignals: true

        onFeedinfo: {
            title = pcdata["title"]
            link = pcdata["link"]
        }

        onEpisodeListData: {
            if (offset == 0) {
                console.log("reloading data")
                podpostsModel.clear()
            }
            for (var i = 0; i < episodes.length; i++) {
                podpostsModel.append(episodes[i])
            }
        }
    }

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent

        VerticalScrollDecorator {}

        AppMenu {
            thispage: "Archive"
        }
        PrefAboutMenu {
            thelink: "https://gitlab.com/cy8aer/podqast/wikis/help-podlist"
        }

        // Tell SilicaFlickable the height of its content.
        contentHeight: page.height
        Column {
            Column {
                id: archivetitle

                width: page.width

                spacing: Theme.paddingLarge
                PageHeader {
                    title: page.title
                    Column {
                        id: placeholder
                        width: Theme.itemSizeExtraSmall / 2
                    }

                    Column {
                        id: prefscol
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: placeholder.right
                    }
                }
            }

            SilicaListView {
                id: archivepostlist
                clip: true
                width: parent.width
                height: page.height - pdp.height - archivetitle.height - searchControls.height
                section.property: 'section'
                section.delegate: SectionHeader {
                    text: section
                    horizontalAlignment: Text.AlignRight
                }

                ViewPlaceholder {
                    enabled: podpostsModel.count == 0
                    text: qsTr("Rendering")
                    hintText: qsTr("Creating items")
                    verticalOffset: -archivetitle.height
                }

                model: ListModel {
                    id: podpostsModel
                }
                delegate: PodpostPostListItem {}
            }

            Column {
                id: searchControls
                width: parent.width
                visible: false

                SearchField {
                    id: episodeSearchField
                    width: parent.width
                    height: visible ? Theme.itemSizeMedium : 0
                    visible: false
                    onTextChanged: {
                        searchtext = episodeSearchField.text
                        get_data()
                    }
                }

                Row {
                    id: episodeSortField
                    width: parent.width
                    visible: false
                    height: visible ? Theme.itemSizeMedium : 0
                    LayoutMirroring.enabled: true

                    IconButton {
                        icon.source: fav_filter ? "image://theme/icon-m-favorite-selected" : "image://theme/icon-m-favorite"
                        icon.color: fav_filter ? Theme.highlightColor : Theme.primaryColor
                        onClicked: {
                            fav_filter = !fav_filter
                            get_data()
                        }
                    }

                    IconButton {
                        icon.source: "image://theme/icon-m-headphone"
                        icon.color: listened_filter < 2 ? Theme.highlightColor : Theme.primaryColor
                        onClicked: {
                            listened_filter++
                            listened_filter = listened_filter % 3
                            get_data()
                        }
                        Icon {
                            source: "image://theme/icon-s-clear-opaque-cross"
                            visible: listened_filter === 0
                            color: Theme.highlightColor
                        }
                    }

                    IconButton {
                        icon.source: order_dir == "asc" ? "image://theme/icon-m-page-up" : "image://theme/icon-m-page-down"
                        icon.color: Theme.highlightColor
                        onClicked: {
                            if (order_dir == "desc")
                                order_dir = "asc"
                            else
                                order_dir = "desc"
                            get_data()
                        }
                    }
                }

                Row {
                    id: settingsrow
                    height: visible ? Theme.itemSizeMedium : 0
                    width: parent.width
                    visible: parent.visible
                    LayoutMirroring.enabled: true

                    IconButton {
                        icon.source: "image://theme/icon-m-cancel"
                        onClicked: {
                            searchControls.visible = false
                        }
                    }

                    IconButton {
                        id: prefsicon
                        icon.source: "image://theme/icon-m-developer-mode"
                        onClicked: {
                            pageStack.push(Qt.resolvedUrl(
                                               "PodcastSettings.qml"), {
                                               "podtitle": title,
                                               "url": url
                                           })
                        }
                    }

                    IconButton {
                        id: searchicon
                        icon.source: "image://theme/icon-m-search"
                        onClicked: {
                            episodeSearchField.visible = !episodeSearchField.visible
                        }
                    }

                    IconButton {
                        id: sorticon
                        icon.source: "image://theme/icon-m-transfer"
                        onClicked: {
                            episodeSortField.visible = !episodeSortField.visible
                        }
                    }
                }
            }
        }
        PlayDockedPanel {
            id: pdp

            IconButton {
                icon.source: "image://theme/icon-m-search"
                onClicked: searchControls.visible = !searchControls.visible
            }
        }
    }
}
