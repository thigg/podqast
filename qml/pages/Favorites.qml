import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    Component.onCompleted: {
        favoritehandler.getFavorites("home")
    }

    Connections {
        target: favoritehandler
        ignoreUnknownSignals: true
        onFavoriteListData: {
            console.debug("got favorite listdata " + offset)
            if (offset === 0)
                archivePostModel.clear()
            for (var i = 0; i < data.length; i++) {
                archivePostModel.append(data[i])
            }
        }
    }

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent

        VerticalScrollDecorator {}

        AppMenu {
            thispage: "Archive"
        }
        PrefAboutMenu {}

        // Tell SilicaFlickable the height of its content.
        contentHeight: page.height

        Column {
            id: archivetitle

            width: page.width

            spacing: Theme.paddingLarge
            PageHeader {
                title: qsTr("Favorites")
            }
        }

        SilicaListView {
            id: archivepostlist
            clip: true
            anchors.top: archivetitle.bottom
            width: parent.width
            height: page.height - pdp.height - archivetitle.height // - podselectgrid.height
            section.property: 'asection'
            section.delegate: SectionHeader {
                text: section
                horizontalAlignment: Text.AlignRight
            }

            model: ListModel {
                id: archivePostModel
            }
            delegate: ArchivePostListItem {}
        }

        PlayDockedPanel {
            id: pdp
        }
    }
}
