import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4
import "../components"

Page {
    id: page

    property bool queryRunning: false
    property string currentQuery: ""

    signal doSearch(string query)
    property alias podcastModel: podslistModel

    SilicaListView {
        id: searcherlist
        anchors.fill: parent
        width: parent.width

        AppMenu {
            thispage: "Discover"
        }
        PrefAboutMenu {}

        header: Column {

            width: parent.width

            PageHeader {
                title: qsTr("Discover by Search")
            }

            SearchField {
                id: searchField
                placeholderText: qsTr("Search")
                anchors.left: parent.left
                width: parent.width

                EnterKey.enabled: text.trim().length > 3
                EnterKey.iconSource: "image://theme/icon-m-search"
                EnterKey.onClicked: {
                    podslistModel.clear()
                    var query = searchField.text.trim()
                    queryRunning = true
                    currentQuery = query
                    doSearch(query)
                }
            }
        }

        model: ListModel {
            id: podslistModel
        }

        delegate: PodcastListItem {}

        ViewPlaceholder {
            enabled: queryRunning
            text: qsTr("Loading results for '%1'").arg(currentQuery)
        }
        ViewPlaceholder {
            enabled: !queryRunning && podslistModel.count === 0
            text: qsTr("No results")
        }
    }

    PlayDockedPanel {}
}
