import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent

        AppMenu {
            thispage: "Inbox"
        }
        PrefAboutMenu {
            thelink: "https://gitlab.com/cy8aer/podqast/wikis/help-inbox"
        }

        Component.onCompleted: {
            inboxhandler.getInboxEntries("home")
        }

        Connections {
            target: inboxhandler
            onInboxData: {
                if (offset === 0)
                    inboxPostModel.clear()
                for (var i = 0; i < data.length; i++) {
                    inboxPostModel.append(data[i])
                }
            }
            onGetInboxPosts: {
                inboxhandler.getInboxEntries(podqast.ifilter)
            }
        }

        Connections {
            target: feedparserhandler
            onRefreshFinished: {
                inboxhandler.getInboxEntries(podqast.ifilter)
            }
        }

        // Tell SilicaFlickable the height of its content.
        contentHeight: page.height

        Column {
            id: inboxtitle

            width: page.width

            spacing: Theme.paddingLarge
            PageHeader {
                title: qsTr("Inbox")
                Column {
                    id: allmovecol
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    IconButton {
                        id: allmoveicon
                        icon.source: "image://theme/icon-m-backup"
                        onClicked: {
                            Remorse.popupAction(
                                        page,
                                        qsTr("Moving all posts to archive"),
                                        function () {
                                            inboxhandler.moveAllArchive()
                                        })
                        }
                    }
                }
            }
        }

        SilicaListView {
            id: inboxpostlist
            clip: true
            anchors.top: inboxtitle.bottom
            width: parent.width
            height: page.height - pdp.height - inboxtitle.height
            section.property: 'section'
            section.delegate: SectionHeader {
                text: section
                horizontalAlignment: Text.AlignRight
            }
            ViewPlaceholder {
                enabled: inboxPostModel.count == 0
                text: qsTr("No new posts")
                hintText: qsTr("Pull down to Discover new podcasts, get posts from Library, or play the Playlist")
                verticalOffset: -inboxtitle.height
            }

            model: ListModel {
                id: inboxPostModel
            }
            delegate: InboxPostListItem {}
        }

        PlayDockedPanel {
            id: pdp
        }
    }
}
