"""
A podcast queue. This does not need to be a singleton. Queue manages the playing queue
It instanciates Podposts via Factory if needed.
"""
import functools
import sys
import os
from typing import Iterator, List

import pyotherside

from podcast import POST_ID_TYPE
from podcast.podpost import PodpostFactory, Podpost

sys.path.append("../")

from podcast.singleton import Singleton
from podcast.factory import Factory
import logging

logger = logging.getLogger(__name__)
queuename = "the_queue"


def sanitize_podpost(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        largs = list(e for e in args)
        podpost = largs[1]
        if type(podpost) != POST_ID_TYPE:
            largs[1] = POST_ID_TYPE(podpost)
        return func(largs[0], largs[1], **kwargs)

    return wrapper


class Queue:
    """
    The podcast Queue. It has a list of podposts
    """
    podposts: List[POST_ID_TYPE]

    def __init__(self):
        """
        Initialization
        """

        self.podposts = []  # Id list of podposts

    def _get_ele_pos(self, index):
        """
        get the position of an element
        """

        pos = 0
        for i in self.podposts:
            if i == index:
                return pos
            pos += 1
        return -1

    def save(self):
        """
        pickle this element
        """

        store = Factory().get_store()
        store.store(queuename, self)

    def _get_top(self) -> Podpost:
        """
        get the top podpost
        """

        if self.podposts == []:
            self.top = None
        else:
            podpost = self.podposts[0]
            self.top = PodpostFactory().get_podpost(podpost)

        return self.top

    @sanitize_podpost
    def insert_top(self, podpost):
        """
        Insert the podpost at top of queue. The post then is triggered to download
        it's audio file.

        podpost: the podpost to be inserted. The post then is trigge
        return: 0 - do nothing, 1 - do stop
        """

        logger.info("queue insert top %d, current length: %d", podpost, len(self.podposts))
        if self.podposts == []:
            self.podposts.append(podpost)
        else:
            if self.podposts[0] == podpost:
                return 0
            pos = self._get_ele_pos(podpost)
            if pos >= 0:
                self.podposts.remove(podpost)
            self.podposts.insert(0, podpost)

        post = self.update_podpost_in_queue(podpost)
        self.save()
        pyotherside.send("needDownload", podpost)
        pyotherside.send("posplay", post.get_position)
        return 1

    @sanitize_podpost
    def insert_next(self, podpost):
        """
        put podpost to the second position
        """

        if type(podpost) != POST_ID_TYPE:
            POST_ID_TYPE(podpost)

        if len(self.podposts) <= 1:
            self.podposts.append(podpost)
        else:
            pos = self._get_ele_pos(podpost)
            if pos >= 0:
                self.podposts.remove(podpost)
            self.podposts.insert(1, podpost)

        self.update_podpost_in_queue(podpost)
        self.save()
        pyotherside.send("needDownload", podpost)

    @sanitize_podpost
    def insert_bottom(self, podpost):
        """
        append a podpost at the end of the queue

        podpost: podpost to be appended
        """
        pos = self._get_ele_pos(podpost)
        if pos >= 0:
            self.podposts.remove(podpost)
        self.podposts.append(podpost)

        self.update_podpost_in_queue(podpost)
        self.save()
        pyotherside.send("needDownload", podpost)

    def update_podpost_in_queue(self, podpost):
        post = PodpostFactory().get_podpost(podpost)
        if post.duration == 0 or (post.duration - post.get_position) / 1000 < 60:
            logger.debug("duration near position")
            post.position = 0
            PodpostFactory().persist(post)
        return post

    @sanitize_podpost
    def remove(self, podpost):
        """
        remove podpost from list
        """

        if podpost in self.podposts:
            self.podposts.remove(podpost)
            post = PodpostFactory().get_podpost(podpost)
            post.delete_file()
            post.state = 0
            PodpostFactory().persist(post)
            if len(self.podposts) > 0 and podpost == self.podposts[0] and len(self.podposts) > 1:
                post1 = PodpostFactory().get_podpost(self.podposts[1])
                pyotherside.send("setpos", post1.position)
        else:
            PodpostFactory().persist(PodpostFactory().get_podpost(podpost))

        self.save()

    @sanitize_podpost
    def download(self, podpost: POST_ID_TYPE) -> Iterator[float]:
        """
        let a podpost download
        @return an iterator over the download progress. You need to iterate this iterator to complete the download
        """

        if podpost in self.podposts:
            post = PodpostFactory().get_podpost(podpost)
            for perc in post.download_audio():
                yield perc
            if podpost == self.podposts[0] and post.file_path:
                pyotherside.send("firstDownloaded", post.file_path)

    def download_all(self):
        """
        Download all posts in queue
        """

        for podpost in self.podposts:
            post = PodpostFactory().get_podpost(podpost)
            if post is None:
                continue
            try:
                if post.file_path:
                    if os.path.isfile(post.file_path):
                        continue
            except:
                pass
            logger.debug("%s: %s needs download", podpost, post.title)
            pyotherside.send("needDownload", podpost)

    def update_position(self, position):
        """
        get the human readable time of podpost
        """

        PodpostFactory().get_podpost(self.podposts[0]).update_position(position=position)

    @sanitize_podpost
    def move(self, podpost, pos):
        """
        Move a podpost to another position (drag/drop)
        podpost: the podpost
        pos: position
        """

        elepos = self._get_ele_pos(podpost)
        if elepos < 0:  # element does not exist in array
            return
        if pos == elepos:  # element is at position
            return

        self.podposts.remove(podpost)
        self.podposts.insert(pos, podpost)
        self.save()

    @sanitize_podpost
    def move_up(self, podpost):
        """
        move podpost one position up
        """

        ind = self._get_ele_pos(podpost)
        if ind > 0:
            self.podposts.remove(podpost)
            self.podposts.insert(ind - 1, podpost)
            self.save()
        return ind - 1

    @sanitize_podpost
    def move_down(self, podpost):
        """
        move podpost one position down
        """

        ind = self._get_ele_pos(podpost)
        if ind < len(self.podposts):
            self.podposts.remove(podpost)
            self.podposts.insert(ind + 1, podpost)
            self.save()

    def get_podposts(self):
        """
        Get a list of all podposts
        """

        for podpost in self.podposts:
            yield podpost

    def get_top_id(self):
        if not self._get_top():
            return None
        return self._get_top().id

    def get_podposts_ids(self):
        yield from self.get_podposts()

    def count(self):
        return len(self.podposts)

    def play(self):
        """
        start playing
        """

        return self._get_top().play()

    def stop(self, position):
        """
        Stop and save position
        """

        podpost = self._get_top()
        podpost.stop(position)

    def position(self, position):
        """
        set top element's position in seconds
        """

        podpost = self._get_top()
        podpost.set_position(position)
        podpost.save()

    def pause(self, position):
        """
        pause situation
        """
        podpost = self._get_top()
        podpost.pause(position)

    def set_duration(self, duration):
        """
        Set duration of element if not set
        """

        self._get_top().set_duration(duration)

    def is_in_queue(self, podpost: Podpost):
        return podpost.id in self.podposts


class QueueFactory(metaclass=Singleton):
    """
    Factory which creates a Queue if it does not exist or gets the pickle
    otherwise it returns the Singleton
    """

    def __init__(self, progname="harbour-podqast"):
        """
        Initialization
        Get the actual paths. Because we do not have a queue by now set have_queue
        to false
        """

        self.queue = None

    def get_queue(self) -> Queue:
        """
        Get the queue
        """

        if not self.queue:
            self.queue = Factory().get_store().get(queuename)
            if not self.queue:
                self.queue = Queue()

        return self.queue

    def get_podposts_objects(self) -> Iterator[Podpost]:
        for postid in self.get_queue().podposts:
            podpost = PodpostFactory().get_podpost(postid)
            if podpost:
                yield podpost
            else:
                logger.warning("Podpost %s is in queue but not found", postid)

    def get_first_podpost_object(self):
        return next(self.get_podposts_objects(), None)
