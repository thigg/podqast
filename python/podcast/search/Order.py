from abc import abstractmethod, ABC
from enum import Enum, auto
from typing import Dict

from peewee import ModelSelect
from podcast.podpost import Podpost


class Direction(Enum):
    ASC = auto()
    DESC = auto()


def handle_order(expression, order: Direction):
    if order == Direction.ASC:
        return expression.asc()
    elif order == Direction.DESC:
        return expression.desc()
    else:
        raise ValueError("Invalid Direction: " + str(order))


class Order(ABC):
    dir: Direction

    def __init__(self, dir: Direction) -> None:
        self.dir = dir

    @abstractmethod
    def apply_order(self, query: ModelSelect, search_env: Dict) -> ModelSelect:
        raise ValueError("do not call abstract method")

    def __eq__(self, other):
        if other is None:
            return False
        if other.__class__ == self.__class__:
            return self.__dict__ == other.__dict__
        else:
            return False

    def __str__(self) -> str:
        return self.__class__.__name__ + "("+str(self.__dict__)+")"


class NoOrder(Order):
    def __init__(self) -> None:
        super().__init__(Direction.ASC)

    def apply_order(self, query: ModelSelect, _) -> ModelSelect:
        return query


class PodpostListInsertDateOrder(Order):
    def apply_order(self, query: ModelSelect, search_env: Dict) -> ModelSelect:
        if not "list_type" in search_env:
            raise ValueError("listtype must be set when ordering by it")
        return query.order_by(handle_order(search_env["list_type"].insert_date, self.dir))


class PodpostPublishedDateOrder(Order):
    def apply_order(self, query: ModelSelect, search_env: Dict) -> ModelSelect:
        return query.order_by(handle_order(Podpost.published, self.dir))
