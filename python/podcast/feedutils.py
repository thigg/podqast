import time
import logging
from typing import Iterator, Callable

import feedparser
from feedparser import FeedParserDict
from podcast import util

logger = logging.getLogger(__name__)


def get_feed_links(feed):
    if "links" in feed:
        return feed.links
    if "channel" in feed and "links" in feed.channel:
        return feed.channel.links
    return []


def get_next_page(feed):
    for link in get_feed_links(feed):
        if "rel" in link and "href" in link and link["rel"] == "next":
            return link["href"]
    return None


def fetch_feed(published, url) -> FeedParserDict:
    assert url
    if url.find("tumblr.com") >= 0:
        agent = util.user_agent2
    else:
        agent = util.user_agent
    feed: FeedParserDict = feedparser.parse(url, agent=agent, modified=time.gmtime(published))
    if feed.status == 304:
        raise NotModified()
    if feed.bozo != 0:
        exc: Exception = feed.bozo_exception
        if type(exc) != feedparser.CharacterEncodingOverride:
            logger.exception(
                "Podcast init: error in parsing feed %s", str(type(exc)), exc_info=exc
            )
            raise FeedFetchingError(exc.message if hasattr(exc, 'message') else "message_missing")
    if "itunes_new-feed-url" in feed.feed and feed.feed["itunes_new-feed-url"] != url:
        raise NewFeedUrlError(feed.feed["itunes_new-feed-url"])
    logger.info(
        "podcast init size of entries: %d", len(feed.entries)
    )
    return feed


def iterate_feed_entries(feed, should_fetch_next_page: Callable[[], bool] = lambda: True) -> Iterator:
    while True:
        for entry in sorted(feed.entries, key=lambda e: e.published_parsed, reverse=True):
            yield entry
        if should_fetch_next_page():
            next_page_href = get_next_page(feed)
            if next_page_href != None:
                logger.info("Found next page: %s", next_page_href)
                feed = fetch_feed(0, next_page_href)
                if feed is None:
                    break
            else:
                break


class FeedFetchingError(BaseException):
    def __init__(self, msg):
        self.message = msg


class NotModified(Exception):
    pass


class NewFeedUrlError(BaseException):
    def __init__(self, url):
        self.url = url
