"""
test the archive
"""

import sys

from peewee import DoesNotExist
from podcast import POST_ID_TYPE
from podcast.util import ilen
from . import setup_archive_with_2_posts

sys.path.append("../python")

from podcast.archive import ArchiveFactory
from podcast.podpost import Podpost, PodpostFactory


def test_insert():
    """
    """

    archive = ArchiveFactory().get_archive()
    entry1, entry2 = setup_archive_with_2_posts()

    result_posts = list(archive.get_podposts())
    assert entry1.id in result_posts
    assert entry2.id in result_posts
    object_ids = [a.id for a in archive.get_podpost_objects('https://freakshow.fm/feed/opus/')]
    assert entry1.id in object_ids
    assert entry2.id in object_ids
    assert 0 == ilen(archive.get_podpost_objects(url_filter='https://i.am.not.corrects/speex'))


def test_duplicate_insert():
    archive = ArchiveFactory().get_archive()
    entry1, entry2 = setup_archive_with_2_posts()
    assert list(archive.get_podposts(sorted_by_date=True)) == [entry2.id, entry1.id]
    archive.insert(entry1.id)
    assert list(archive.get_podposts(sorted_by_date=True)) == [entry1.id, entry2.id]


def test_string_insert():
    archive = ArchiveFactory().get_archive()
    entry1, entry2 = setup_archive_with_2_posts()
    archive.insert(str(entry1.id))

def test_invalid_insert():
    archive = ArchiveFactory().get_archive()
    assert PodpostFactory().get_podpost(0) == None
    try:
        archive.insert(0)
        assert False
    except DoesNotExist:
        pass

def test_get_archives():
    """
    Test listing of podposts
    """
    a = ArchiveFactory().get_archive()
    setup_archive_with_2_posts()
    count = 0
    for post in a.get_podposts():
        assert type(post) == POST_ID_TYPE
        assert type(PodpostFactory().get_podpost(post)) == Podpost
        count += 1
    posts = list(a.get_podpost_objects())
    assert len(posts) == 2
    for post in posts:
        assert type(post) == Podpost
    assert ilen(a.get_podpost_objects(filter_favorite=True)) == 0
    assert ilen(a.get_podpost_objects(url_filter="http://i.am.wrong")) == 0
    assert ilen(a.get_podpost_objects(url_filter="http://i.am.wrong",filter_favorite=True)) == 0
    assert ilen(a.get_podpost_objects(url_filter="https://freakshow.fm/feed/opus/")) == 2
    assert ilen(a.get_podpost_objects(url_filter="https://freakshow.fm/feed/opus/",filter_favorite=True)) == 0
    for post in a.get_podpost_objects():
        post.favorite = True
        PodpostFactory().persist(post)
    assert ilen(a.get_podpost_objects(url_filter="https://freakshow.fm/feed/opus/",filter_favorite=True)) == 2
    assert ilen(a.get_podpost_objects(filter_favorite=True)) == 2
    assert ilen(a.get_podpost_objects(url_filter="http://i.am.wrong",filter_favorite=True)) == 0

    assert count == 2
