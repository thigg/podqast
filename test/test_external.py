import sys

from podcast.external import ExternalFactory
from podcast.util import ilen
from test import setup_and_get_2_posts

sys.path.append("../python")

def test_external():
    """
    """

    external = ExternalFactory().get_external()
    entry1, entry2 = setup_and_get_2_posts()

    assert ilen(external.get_podposts()) == 0
    external.insert(entry1.id,"abc")
    assert list(external.get_podposts()) == [entry1.id]
    assert list(external.get_podposts_objects()) == [("abc",entry1)]
    assert external.has_file("abc")
    assert not external.has_file("abcd")
    assert not external.has_file("")
    assert not external.has_file(None)
    external.remove_by_filename("abcd")
    external.remove_by_filename("abc")
    assert not external.has_file("abc")
    external.insert(entry2.id,"abcd")
    external.remove_podpost(entry1.id)
    assert external.has_file("abcd")
    assert list(external.get_podposts()) == [entry2.id]
    external.remove_podpost(entry2.id)
    assert list(external.get_podposts()) == []
