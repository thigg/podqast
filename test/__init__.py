import os
import tempfile
import types
import sys
from unittest.mock import Mock
import logging

sys.path.append("../python")

from httpretty import httprettified, HTTPretty
from httpretty.utils import utf8

from test.conftest import cleanup_podcast

logger = logging.getLogger(__name__)

def mock_pyotherside():
    module_name = "pyotherside"
    module = types.ModuleType(module_name)
    sys.modules[module_name] = module
    module.send = Mock(name=module_name + '.send')

mock_pyotherside()

from podcast.archive import ArchiveFactory
from podcast.inbox import InboxFactory
from podcast.podcast import Podcast
from podcast.podpost import PodpostFactory

os.environ["PODQAST_HOME"] = tempfile.mkdtemp()
logger.info("tempdir: %s",os.environ["PODQAST_HOME"])

from podcast.constants import Constants
from podcast.data_migration import setup_db
Constants()
setup_db()

def setup_archive_with_2_posts():
    archive = ArchiveFactory().get_archive()
    entry1, entry2 = setup_and_get_2_posts()
    archive.insert(entry1.id)
    archive.insert(entry2.id)
    return entry1, entry2

def setup_inbox_with_2_posts():
    inbox = InboxFactory().get_inbox()
    entry1, entry2 = setup_and_get_2_posts()
    inbox.insert(entry1.id)
    inbox.insert(entry2.id)
    return entry1, entry2

@httprettified
def setup_and_get_2_posts():
    logger.info("setup with dummy feed")
    HTTPretty.register_uri(HTTPretty.GET, 'https://freakshow.fm/feed/opus/',
                           body=read_testdata('testdata/freakshow.rss'), adding_headers=xml_headers)
    cleanup_podcast()
    podcast, episodes = Podcast.create_from_url('https://freakshow.fm/feed/opus/')
    assert podcast
    entry1 = podcast.get_entry(podcast.entry_ids_old_to_new[0])
    assert entry1
    PodpostFactory().persist(entry1)
    entry2 = podcast.get_entry(podcast.entry_ids_old_to_new[1])
    assert entry2
    PodpostFactory().persist(entry2)
    logger.info("persisted dummy feed")
    return entry1, entry2


xml_headers = {"content-type": "application/rss+xml; charset=UTF-8"}


def read_testdata(filename):
    data = ""
    with open(os.path.join(os.path.dirname(__file__), filename), 'r') as file:
        data = file.read()
    return utf8(data)